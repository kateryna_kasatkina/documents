#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"


fd_set sockets;




int __cdecl main(void) 
{

	FD_ZERO(&sockets);
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if ( iResult != 0 ) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	printf("Server started...\n");
	// Accept a client socket
	sockaddr_in s;
	int sz = sizeof(s);
	for(int i=0; i<1; i++)
	{
		ClientSocket = accept(ListenSocket, (sockaddr*)&s, &sz);

		if (ClientSocket == INVALID_SOCKET) {
			printf("accept failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			WSACleanup();
			return 1;
		}
		FD_SET(ClientSocket,&sockets);
		printf("Client connected (%s).\r\n", inet_ntoa(s.sin_addr));
	}
	// No longer need server socket
	closesocket(ListenSocket);
	//getchar();
	// Receive until the peer shuts down the connection
	printf("Cliens connected. Ready to listen...\n");
	do {
		TIMEVAL t;
		t.tv_usec = 0;
		t.tv_sec = 1;
		iResult = 1;
		
		fd_set tmp_sockets = sockets;
		//FD_SET(ClientSocket,&sockets);
		if(select(0,&tmp_sockets,NULL,NULL,&t))
		{
			for(int i=0; i<tmp_sockets.fd_count; i++)
			{
				ZeroMemory(recvbuf,512);
				iResult = recv(tmp_sockets.fd_array[i], recvbuf, recvbuflen, 0);
				if (iResult > 0) {
					printf("Mess(sock - %d): %s\n", tmp_sockets.fd_array[i], recvbuf);
				}
				else if (iResult == 0)
					printf("Connection closing...\n");
				else  {
					printf("recv failed with error: %d\n", WSAGetLastError());
					closesocket(ClientSocket);
					WSACleanup();
					return 1;
				}
			}
		}//else printf("Mess file: %s line: %d", __FILE__, __LINE__);
	} while (iResult > 0);

	// shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();

	return 0;
}
/*
int sendmy(SOCKET ConnectSocket){

}*/