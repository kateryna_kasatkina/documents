#include <iostream>
using namespace std;
#include "Object.h"
#include "Ellipse.h"
#include "Rectangle.h"
void main()
{

	cout << endl << endl;
	Object obj1;
	Ellipse obj2;
	Rectangle obj3;

	obj1.square();
	obj2.square();
	obj3.square();
	//====================

	

	obj2.setX(3);
	obj2.setY(5);
	obj2.square();

	

	//����� ������������� ������ ����� ��������������
	((Object)obj2).square();
	cout << " 2 ============================================"<<endl;
	
	Object arr[3];
	arr[0] = obj1;
	arr[1] = obj2;
	arr[2] = obj3;

	for(int i=0; i<3 ;i++)
		arr[i].square();

	

	cout << " 3 ============================================"<<endl;
	
	Object* p_obj1 = new Object;
	Ellipse* p_obj2 = new Ellipse;
	Rectangle* p_obj3 = new Rectangle;

	p_obj1->square();
	p_obj2->square();
	p_obj3->square();

	



	delete p_obj1;
	delete p_obj2;
	delete p_obj3;

	
	cout << " 4 ============================================"<<endl;


	Object* ob_arr[3];

	ob_arr[0] = new Object;
	ob_arr[1] = new Ellipse;
	ob_arr[2] = new Rectangle;

    for(int i=0; i<3; i++)
		ob_arr[i]->square();

	((Ellipse*)ob_arr[1])->square();

	for(int i=0; i<3; i++)
		delete ob_arr[i];


	
}

/*

          +----+
Object    |XXXX|
          +----+ 

          +--------+
Rectangle |XXXXRRRR|
		  +--------+

Object o(.....);
Rectangle r(.....);

r = o;

=======================

Object o(.....);
Rectangle r = o;

*/