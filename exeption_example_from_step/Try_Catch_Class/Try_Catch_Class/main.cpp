#include <iostream>
#include "Exception.h"



void test_fun(int x)
{
	if (x < 0)
	{
		std::cout << x << std::endl;
		throw Exception("my message!!!!!!");
	}
	else if(x > 0)
		 {
			 std::cout << x*x << std::endl;
			 throw 2;
		 }
		 //else throw 3.6;

	 std::cout << "++++++++++++++++" << std::endl;
	 throw "3454";
	 std::cout << "----------------" << std::endl;
}
void main()
{
	using std::cout;
	using std::endl;
	///try catch throw
	cout << __FILE__ << ":" << __LINE__<<endl;

	

	try
	{
		test_fun(-10);
		//cout << "test_fun(-10); OK" << endl;
		//test_fun(200);
		//cout << "test_fun(200); OK" << endl;
		test_fun(0);
		cout << "test_fun(0); OK" << endl;

		
	}
	
	catch(int error)
	{
		cout << "Int Exception:" << error << endl;
	}
	catch (double error)
	{
		cout << "double Exception:" << error << endl;

	}
	catch(Exception e)
	{
		cout << "Exception: "<<e.get_message()<<endl;
	}
	catch(...)
	{
		cout << "Unknown Exception:"  << endl;

	}


	//int cout = 6;

	
}