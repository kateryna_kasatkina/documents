#pragma once
#include <iostream>
#include <string>
/*4 ����������� ��������� ���������(1)
	- =
	- ==(!=)
	- <<
	- >>*/
using std::string;
using std::cout;
using std::endl;
using std::ostream;
using std::istream;
class school_student
{
	friend ostream& operator<<(ostream& os, school_student&a);
	friend istream& operator >> (istream& is, school_student&a);
protected:
	string firstname;
	string lastname;
	int overal_mark;
public:
	school_student()
	{
		firstname = "";
		lastname = "";
		overal_mark = 0;
	}
	school_student(string aFirst, string aLast, int aOveral_mark) :firstname(aFirst), lastname(aLast), overal_mark(aOveral_mark) {}
	school_student(school_student &a)
	{
		firstname = a.firstname;
		lastname = a.lastname;
		overal_mark = a.overal_mark;
	}
	 ~school_student() {}
	 string GetName()const
	{
		return firstname + " " + lastname;
	}
	void set_firstname(string aFirstname)
	{
		firstname = aFirstname;
	}
	void set_lastname(string aLastname)
	{
		lastname = aLastname;
	}
	void set_overal_mark(int aOveral_mark)
	{
		overal_mark = aOveral_mark;
	}
	string get_firstname()const
	{
		return firstname;
	}
	string get_lastname()const
	{
		return lastname;
	}
	int get_overal_mark()const
	{
		return overal_mark;
	}
	school_student operator=(school_student&a)
	{
		firstname = a.firstname;
		lastname = a.lastname;
		overal_mark = a.overal_mark;
		return *this;
	}
	bool operator==(school_student&a)
	{
		if (firstname == a.firstname&&lastname == a.lastname&&overal_mark == a.overal_mark)
			return true;
		else
			return false;
	}
	bool operator!=(school_student&a)
	{
		if (firstname != a.firstname||lastname != a.lastname||overal_mark != a.overal_mark)
			return true;
		else
			return false;
	}
};
ostream& operator<<(ostream& os, school_student&a)
{
	os << "Firstname: "<<a.firstname << endl;
	os << "Lastname: "<<a.lastname << endl;
	os << "Overal mark: "<<a.overal_mark << endl;
	return os;
}
istream& operator >> (istream& is, school_student&a)
{
	//cout << "input Firstname: " << endl;
	is >>  a.firstname ;
	//cout << "input Laststname: " << endl;
	is >>  a.lastname ;
	//cout << "input overal mark: " << endl;
	is >>  a.overal_mark;

	return is;
}