#include "tree.h"
#include "student.h"
using namespace std;
tree::tree()
{
	root = nullptr;
}

int tree::min()
{
	student *tmp = root;
	while (tmp && tmp->get_left()) {
		tmp = tmp->get_left();
	}

	return tmp ? tmp->get_year() : 0;


}

/*void Vocabulary::print()
{
	print_helper_straight(root);
}

void Vocabulary::print_helper_simmetric(Element * el)	//min - root - max  
{
	if (el->get_left() )
		print_helper_simmetric(el->get_left());
	cout << el->get_value() << " ";
	if (el->get_right())
		print_helper_simmetric(el->get_right());
}

void Vocabulary::print_helper_straight(Element * el)	//root - min - max	
{
	cout << el->get_value() << " ";
	if (el->get_left())
		print_helper_straight(el->get_left());
	if (el->get_right())
		print_helper_straight(el->get_right());
}

void Vocabulary::print_helper_reversed(Element * el)	//root - max - min	
{
	cout << el->get_value() << " ";
	if (el->get_right())
		print_helper_reversed(el->get_right());
	if (el->get_left())
		print_helper_reversed(el->get_left());
}*/

void tree::print()
{
	//cout << "root: "<<root->get_name() << endl;
	print_helper_simmetric(root);
}
int tree::print_helper_simmetric(student * el)
{
	/*if (el->get_left())
		print_helper(el->get_left());
	cout <<"Name: "<< el->get_name() << endl;
	cout <<"Surname: "<< el->get_surname() << endl;
	cout <<"Age: "<<el->get_year()<<endl;
	cout << "---------------------" << endl;
	if (el->get_right())
		print_helper(el->get_right());*/

	if (el->get_left() )
		print_helper_simmetric(el->get_left());
	//cout << el->get_value() << " ";
	cout << "Name: " << el->get_name() << endl;
	cout << "Surname: " << el->get_surname() << endl;
	cout << "Age: " << el->get_year() << endl;
	cout << "---------------------" << endl;
	if (el->get_right())
		print_helper_simmetric(el->get_right());
	return 0;
}

void tree::add(student *val) {
	if (!root)
		root = val;
	else
	{
		student *tmp = root;
		while (tmp)
		{
			if (val < tmp)
				if (tmp->get_left() == NULL)
				{
					tmp->set_left(val);
					break;
				}
				else tmp = tmp->get_left();
			else {
				if (val > tmp)
					if (tmp->get_right() == NULL)
					{
						tmp->set_right(val);
						break;
					}
					else tmp = tmp->get_right();
				else tmp = NULL;
			}
		}
	}
}


void  tree::add_using_data(string aName, string aSurname, int aYear_birth)
{
	if (!root)
		root = new student(aName, aSurname, aYear_birth);
	else
	{
		student *tmp = root;
		while (tmp)
		{
			if (aYear_birth < tmp->get_year())
				if (tmp->get_left() == NULL)
				{
					tmp->set_left(new student(aName, aSurname, aYear_birth));
					break;
				}
				else tmp = tmp->get_left();
			else {
				if (aYear_birth > tmp->get_year())
					if (tmp->get_right() == NULL)
					{
						tmp->set_right(new student(aName, aSurname, aYear_birth));
						break;
					}
					else tmp = tmp->get_right();
				else tmp = NULL;
			}
		}
	}
}