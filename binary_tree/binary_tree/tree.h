#pragma once
#include <string>
#include <iomanip>
#include "student.h"

using namespace std;
class student;
class tree
{
	student *root;
public:
	tree();

	int min();
	
	void print();
	
	int print_helper_simmetric(student * el);

	void add(student *val);

	void add_using_data(string aName, string aSurname, int aYear_birth);
	
	
};