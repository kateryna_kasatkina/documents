#include "student.h"
#include <string>
using namespace std;
student::student()
{
	string a = "no data";
	name = a;
	surname = a;
	year_birth = 0;
	left = right = nullptr;
}
student::student(string aName, string aSurname, int aYear_birth)
{
	name = aName;
	surname = aSurname;
	year_birth = aYear_birth;
	left = right = nullptr;
}
student::student(student &a)
{
	name = a.name;
	surname = a.surname;
	year_birth = a.year_birth;
	left = right = nullptr;
}
void student::set_name(string aName)
{
	name = aName;
}
void student::set_surname(string aSurname)
{
	name = aSurname;
}
void student::set_year(int aYear_birth)
{
	year_birth = aYear_birth;
}
string student::get_name()
{
	return name;
}
string student::get_surname()
{
	return surname;
}
int student::get_year()
{
	return year_birth;
}
void student::set_left(student *aleft)
{
	left = aleft;
}
void student::set_right(student *aright)
{
	left = aright;
}
student *student::get_left()
{
	return left;
}
student *student::get_right()
{
	return right;
}