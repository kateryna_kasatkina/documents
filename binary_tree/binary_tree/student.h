#pragma once
#include <iostream>
#include <string>
using namespace std;
class student
{
	student *left;
	student *right;
	string name;
	string surname;
	int year_birth;
public:
	student();
	
	student(string aName, string aSurname, int aYear_birth);
	
	student(student &a);
	
	void set_name(string aName);
	
	void set_surname(string aSurname);
	
	void set_year(int aYear_birth);
	
	string get_name();
	
	string get_surname();
	
	int get_year();
	
	void set_left(student *aleft);
	
	void set_right(student *aright);
	
	student *get_left();
	
	student *get_right();

	//Point operator=(const Point& aPoint)
	student operator>( student *a)
	{
		if (year_birth > a->year_birth)
			return *this;
		else return  *a;
	}
	student operator<(student *a)
	{
		if (year_birth < a->year_birth)
			return *this;
		else return  *a;
	}
};