#include <iostream>
#include <cstring>
#include <stdio.h>
#include "INGREDIENT.h"
#include "recipe.h"
#include "BOOK.h"
using namespace std;
RECIPE::RECIPE()
{
	int number_recipe = 0;
	char *name = nullptr;
	char *type = nullptr;
	char *prep = nullptr;
	INGREDIENT **arr_ing = nullptr;
	int quan_ing = 0;
}

RECIPE::RECIPE(int &rsize_arr, char *str1, char *str2, char *str3)
{
	int number_recipe = rsize_arr - 1;
	char *name = str1;
	char *type = str2;
	char *prep = str3;
	INGREDIENT *arr_ing = nullptr;
	int quan_ing = 0;
}
void RECIPE::set_number_recipe(int ai)
{
	number_recipe = ai;
}
void RECIPE::get_number_recipe()
{
	cout << number_recipe << endl;
}

void RECIPE::set_name_recipe(char str1[])
{
	int size_name = strlen(str1);
	name = new char[size_name + 1];
	strcpy_s(name, size_name + 1, str1);
}
void RECIPE::get_name_recipe()
{
	cout << name << endl;
}
void RECIPE::set_type_recipe(char str2[100])
{
	int sz = strlen(str2);
	type = new char[sz + 1];
	strcpy_s(type, sz + 1, str2);
}
void RECIPE::get_type_recipe()
{
	cout << type << endl;
}
void RECIPE::set_prep_recipe(char str3[100])
{
	int sz = strlen(str3);
	prep = new char[sz + 1];
	strcpy_s(prep, sz + 1, str3);
}
void RECIPE::get_prep_recipe()
{
	cout << prep << endl;
}

void RECIPE::show_ingredients()
{
	for (int i = 0; i <quan_ing; i++)
	{
		arr_ing[i]->get_number_ing();
		arr_ing[i]->get_name_ing();
		arr_ing[i]->get_weight();
		cout << "-------------------------\n";
	}
}
void RECIPE::fill_new_recipe(RECIPE **arr_recipe, int i)
{
	arr_recipe[i]->set_number_recipe(i);
	arr_recipe[i]->set_name_recipe("recipe");
	arr_recipe[i]->set_type_recipe("cold");
	arr_recipe[i]->set_prep_recipe("boill");
	arr_recipe[i]->add_ing();
}
void RECIPE::add_ing()
{
		
		INGREDIENT **arr_ing_new = new INGREDIENT*[quan_ing+1];//1. new array of pointers
		for (int i = 0; i <quan_ing + 1; i++)
		{
			if (i == quan_ing )//3.adding a new element
			{
				arr_ing_new[i] = new INGREDIENT;
				arr_ing_new[i]->fill_new_ing(arr_ing_new, i);
			}
			if(quan_ing!=0&& i != quan_ing)
			arr_ing_new[i] = arr_ing[i];//2.copy info to the new array of pointers

		}
		quan_ing++;
		delete[]arr_ing;//4.delete old array of pointers
		arr_ing = arr_ing_new;//attach new array of pointers
	}

void RECIPE::delete_ing()
{
	int point_delete = 0;
	cout << "input number of ingredient for deleting\n";
	cin >> point_delete;
	cin.get();
	INGREDIENT **arr_ing_new = new INGREDIENT*[quan_ing -1];
	int i = 0;
	while( i<point_delete)
	{
		arr_ing_new[i] = arr_ing[i];
		i++;
	}
	if (i == point_delete)
	{
		for(int j=i+1;j<quan_ing;j++,i++)
			arr_ing_new[i] = arr_ing[j];
	}
	quan_ing--;
	delete[]arr_ing;
	arr_ing = arr_ing_new;
}

void RECIPE::show_list_ingredients()
{
	for (int i = 0; i < quan_ing; i++)
	{
		arr_ing[i]->get_name_ing();
	}
}
RECIPE::~RECIPE()
{
	delete[]name;
	delete[]type;
	delete[]prep;

}