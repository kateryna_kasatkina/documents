#pragma once
class RECIPE
{
private:
	int number_recipe;
	char *name;
	char *type;
	char *prep;
	INGREDIENT **arr_ing;
	int quan_ing;
public:
	RECIPE();
	
	RECIPE(int &rsize_arr, char *str1, char *str2, char *str3);
	
	void set_number_recipe(int ai);
	
	void get_number_recipe();
	
    void set_name_recipe(char str1[]);
	
	void get_name_recipe();
	
	void set_type_recipe(char str2[100]);
	
	void get_type_recipe();
	
	void set_prep_recipe(char str3[100]);
	
	void get_prep_recipe();

	void show_ingredients();

	void fill_new_recipe(RECIPE **arr_recipe, int i);

	void add_ing();

	void delete_ing();

	void show_list_ingredients();

	~RECIPE();
	
};