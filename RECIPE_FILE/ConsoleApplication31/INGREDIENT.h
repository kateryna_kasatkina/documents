#pragma once
#include<iostream>
#include <fstream>
using namespace std;
class INGREDIENT
{
private:
	int number_ing;
	char *ing;
	int weight;
public:
	INGREDIENT();
	
	void set_number_ing(int ai);
	
	void get_number_ing();
	
	void set_name_ing(char aIng[]);

	void get_name_ing();
	
	void set_weight(int aWeight);
	
	void get_weight();
	
	void fill_new_ing(INGREDIENT **arr_ing_new, int i);

	void write_to_file(char *file_name)
	{
		fstream f(file_name, ios::binary | ios::app);
		int sz = strlen(ing);
		f.write(reinterpret_cast<char*>(&sz), sizeof(sz));
		f.write(ing, sz);
	}
	
};