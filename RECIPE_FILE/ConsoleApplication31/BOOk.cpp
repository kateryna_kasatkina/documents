#include <iostream>
#include <cstring>
#include <stdio.h>
#include "INGREDIENT.h"
#include "recipe.h"
#include "BOOK.h"
void RECIPE_BOOK::add_recipe()
{
	RECIPE **arr_recipe_new = new RECIPE*[quan_recipe+1];
	for (int i = 0; i < quan_recipe + 1; i++)
	{
		if (i == quan_recipe)
		{
			arr_recipe_new[i] = new RECIPE;
			arr_recipe_new[i]->fill_new_recipe(arr_recipe_new, i);
		}
		if (quan_recipe != 0 && i != quan_recipe)
			arr_recipe_new[i] = arr_recipe[i];
	}
	quan_recipe++;
	delete[]arr_recipe;
	arr_recipe = arr_recipe_new;
}

/*void RECIPE::delete_ing()
{
	int point_delete = 0;
	cout << "input number of ingredient for deleting\n";
	cin >> point_delete;
	cin.get();
	INGREDIENT **arr_ing_new = new INGREDIENT*[quan_ing -1];
	int i = 0;
	while( i<point_delete)
	{
		arr_ing_new[i] = arr_ing[i];
		i++;
	}
	if (i == point_delete)
	{
		for(int j=i+1;j<quan_ing;j++,i++)
			arr_ing_new[i] = arr_ing[j];
	}
	quan_ing--;
	delete[]arr_ing;
	arr_ing = arr_ing_new;
}*/

void RECIPE_BOOK::delete_recipe()
{
	int point_delete = 0;
	cout << "input number of recipe for deleting\n";
	cin >> point_delete;
	cin.get();
	RECIPE **arr_recipe_new = new RECIPE*[quan_recipe - 1];
	int i = 0;
	while (i<point_delete)
	{
		arr_recipe_new[i] = arr_recipe[i];
		i++;
	}
	if (i == point_delete)
	{
		for (int j = i + 1; j<quan_recipe; j++, i++)
			arr_recipe_new[i] = arr_recipe[j];
	}
	quan_recipe--;
	delete[]arr_recipe;
	arr_recipe = arr_recipe_new;
}
void RECIPE_BOOK::show_recipes()
{
	for (int i = 0; i < quan_recipe; i++)
	{
		arr_recipe[i]->get_number_recipe();
		arr_recipe[i]->get_name_recipe();
		arr_recipe[i]->get_type_recipe();
		arr_recipe[i]->get_prep_recipe();
		arr_recipe[i]->show_list_ingredients();
		cout << "-------------------" << endl;
	}
}
void  RECIPE_BOOK::show_list_recipe()
{
	for (int i = 0; i<quan_recipe; i++)
		arr_recipe[i]->get_name_recipe();
}