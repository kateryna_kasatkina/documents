#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
class user {
protected:
	string name;
	string product;
	int price;
public:
	user() 
	{
		name = "";
		product = "";
		price = 0;
	}
	user(string aName,string aProduct,int aPrice)
	{
		name = aName;
		product = aProduct;
		price = aPrice;
	}
	void set_product(string aProduct)
	{
		product = aProduct;
	}
	string get_product()
	{
		return product;
	}
	void set_price(int aPrice)
	{
		price = aPrice;
	}
	int get_price()
	{
		return price;
	}
	
	void create_report()
	{
		string file_name = "report_user.txt";
		ofstream f;
		f.open(file_name);
		//fstream f(file_name, ios::binary|ios::app);
		//write to file string-product
		size_t size_product = product.size();
		f.write(reinterpret_cast<char*>(&size_product), sizeof(size_product));
		f.write(product.c_str(), size_product);
		//write to file int-price
		f.write(reinterpret_cast<char*>(&price), sizeof(price));
		f.close();

	}
	/*void DBMS::read_from_file(char * file_name)
	{
		ifstream F(file_name, ios::binary);
		// checking if file was successfully opened, othervice generating exception:
		if (!F)
			throw "DBMS: Can't open file for reading";
		try
		{
			factory.read_from_file(F);
		}
		catch (char *msg)
		{
			std::cout << msg << "\n";
		}
		F.read(reinterpret_cast<char*>(&salons_counter), sizeof(int));	//salons_counter reading
		F.read(reinterpret_cast<char*>(&car_catalog_counter), sizeof(int));	//car_catalog_counter reading
		F.read(reinterpret_cast<char*>(&size_salons), sizeof(int));	//size_salons reading
		*/
	void read_from_file()
	{
		string file_name = "report_user.txt";
		ifstream f;
		f.open(file_name);
		//F.read(reinterpret_cast<char*>(&salons_counter), sizeof(int));	//salons_counter reading
		string something = "";
		f.read(reinterpret_cast<char*>(&something), sizeof(something));
		cout << something;
		f.close();
	}
};
