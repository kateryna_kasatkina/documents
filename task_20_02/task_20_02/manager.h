#pragma once
#include "user.h"
class manager :public user
{
	string state_of_inquiry;
public:
	manager(string aName, string aProduct, int aPrice, string aState_of_inquiry):user(aName,aProduct,aPrice), state_of_inquiry(aState_of_inquiry){}
	string get_state_of_inquiry()
	{
		return state_of_inquiry;
	}
	void set_in_progress()
	{
		state_of_inquiry = "in_progress";
	}
	void set_denyed()
	{
		state_of_inquiry = "denyed";
	}
	void set_done()
	{
		state_of_inquiry = "done";
	}
};
