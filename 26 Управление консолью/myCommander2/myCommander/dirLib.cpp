#include "dirLib.h"
#include <io.h>
#include <iostream>
using std::cout;
#include<string>
#include <Windows.h>
void fillDirectoryArray(char ***aArr, int &aArrSz, char* aDirectory){
	_finddata_t  c_file;
	intptr_t h;

	*aArr = new char*[2000];

	char tmpPath[260]= {0};
	strcpy_s(tmpPath,aDirectory);
	strcat_s(tmpPath, "/*.*");
	h = _findfirst(tmpPath,&c_file);
	int arrPos = 0;
	if( h != -1 )
	{
		do
		{
			(*aArr)[arrPos] = new char[260];
			strcpy_s((*aArr)[arrPos++], 260, c_file.name);
			//if((c_file.attrib & _A_SUBDIR) ==_A_SUBDIR) 
			//if((c_file.attrib & _A_SUBDIR) == 0)
			/*
			if((c_file.attrib & _A_SUBDIR)==_A_SUBDIR)
			cout << "<DIR>  "; 
			else 
			cout << "       "; 
			cout <<c_file.name <<"  "<<c_file.size<<  endl;*/
		}while(_findnext(h,&c_file) == 0);
	}
	aArrSz = arrPos;
	_findclose(h);
}

void showDirectory(char **aArr, int aArrSz, 
	int aStartPosition, int aActiveItem){
	
		//system("cls");
		int activeColor = BACKGROUND_RED | BACKGROUND_GREEN |BACKGROUND_BLUE|BACKGROUND_INTENSITY;
		int defaultColor = FOREGROUND_BLUE |FOREGROUND_GREEN|FOREGROUND_RED|FOREGROUND_INTENSITY;

		HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);  

		for(int i = 0; i < 25; i++){
			if(i == aActiveItem)
				SetConsoleTextAttribute(hStdOut,activeColor);
				

			COORD pos;

			pos.X = 2;
			pos.Y = i;
			SetConsoleCursorPosition(hStdOut, pos);
			//Sleep(50);
			printf("%-40s",aArr[aStartPosition + i]);
			//cout << aArr[aStartPosition + i];
			SetConsoleTextAttribute(hStdOut,defaultColor);

		}

}