#pragma once
#include "String.h"
#include "date.h"
using namespace std;
enum position { mgr, dir }; //enum type created for workers
class employee
{
protected:
	int ID[2]; //ID ������ � ID ���������a
	String name;
	String surname;
	String phone;
	date  birth;
	position type;
public:
	employee();//default constructor

	employee(String aSurname, String aName, int IDsalon, int IDemployee, int aDay, int aMonth, int aYear, String aPhone, position aType);//overloaded constructor

	employee(const employee &a);//copy constructor

	void set_name(String aName);

	void set_surname(String aSurname);

	void set_phone(String aPhone);

	void set_ID_employee(int IDemployee);

	void set_ID_salon(int IDsalon);

	void set_birth_date(int aDay, int aMonth, int aYear);

	void set_position(position atype);

	void show_employee();

	int get_salon_ID();

	int get_employee_ID();

	position get_position()const;

	friend ostream& operator<<(ostream& os, const employee& dt);
};

