#include <iostream>
#include "String.h"
#include "Employee.h"
#include "date.h"
#include "inquiry.h"
using namespace std;
ostream& operator << (ostream& os, const String &aString)
{
	if (aString.get_string() != nullptr)
		os << aString.get_string();
	return os;
}
ostream& operator << (ostream& os, date &aDate)
{
	os << aDate.day << "." << aDate.month << "." << aDate.year << endl;
	return os;
}
int main()
{
	employee x;
	x.set_name("katya");
	x.set_surname("kasatkina");
	x.set_phone("123414534");
	x.set_birth_date(6, 4, 1987);
	x.set_ID_salon(1);
	x.set_ID_employee(14);
	x.set_position(mgr);
	x.show_employee();
	cout << "---------------" << endl;
	//Employee(string aSurname, string aName, int IDsalon, int IDemployee,
	//short aDay, short aMonth, short aYear, string aPhone, position aType);
	//Employee m("kasatkina", "xxxxx",1,12,6,4,1987,"123456",mgr);
	//Employee m = x;
	//m.show_employee();
	cout << x;

	/*inquiry::inquiry
(int aSalonID,
	int aManagerID,
	int aClientID,
	int aCarID, //��� ��� ����
	int aInquiryID,
	//int car_ID, // car_ID - ������? ��, ������, ������
	int aDay,
	int aMonth,
	int aYear)
	/*inquiry b;
	cout << b;*/
}