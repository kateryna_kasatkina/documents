/*#pragma once
#include "String.h"
#include "inquiry.h"
#include "Person.h"

/*������ ������� (��� ������������� � �� �������������):
added - ����� ������, ��� �� ���������������
changed - ������ ������, �� ������ ���� �������
synchronyzed - ������ ������, ������ �� ���������� � ������� ��������� �������������
*/
/*enum client_status { added, changed, synchronyzed };


/*������������ � �������  manufacturerDB � salonDB, ������ ���������� � �������� � ��� ������� */
/*class client
{
private:
	int ID[2];					//ID ������ � ID �������
	client_status status;		//������ ������� (��.����)
	Person clients_person;		//��� ������ � ������� ��������� ����� ����� Person (��� �������)
	int list_sz;				//���������� �������� � ������� �������
	inquiry *inquirys_list;		//������ �������� ������� �������

	friend class DataBase;		//�.�. ���������� ������ DataBase �������� � ������������ ����� ������????

public:
	client();							//default constructor
	client(int aSalon_ID, int aClient_ID, Person aClient);	//overloaded constructor
	client(const client &aClient);		//copy constructor
	~client();							//destructor

public:
	//inline �� ���� ���������� � ���� ����������, ��� ����� - ������ � ����������!
	inline int get_salon_ID()const;
	inline int get_client_ID()const;
	inline client_status get_status()const;
	inline int get_list_sz()const;			//���������� ���������� �������� � ����� �������
	inquiry *get_inquiry_list()const;		//���������� ��������� �� ������ �������

											/*set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
	/*void set_ID(int aSalon_ID, int aClient_ID);
	void set_salon_ID(int aSalon_ID);
	void set_client_ID(int aClient_ID);
	void set_client(Person aClient);
	void set_client(String aSurname, String aName, String aPatronymic, short aDay = 0, short aMonth = 0, short aYear = 0, String aPhone = nullptr);
	void set_client_status(client_status aStstus);	//�������� (������ ������), ���� ����� ����� - ���� ��������
													/*�������� ������ �� ������, �������� ��� ID: ID ������(1-� ������� ID �������)
													- ID ���������=0(����� ��������� ����� ������� ������ ��������) -
													ID ������� (2-� ������� ID �������) �
													ID ���� (�� ��������� ������) -
													ID ������ (���������� ����� � ������ �������� ����� �������)
													*/
	/*void add_inquiry(int car_ID);
	client operator= (const client &aClient);
	client operator+ (inquiry aInquairy);	//�������� ������� ������ (��� ������������)
	client operator+=(inquiry aInquairy);	//�������� ������� ������ (��� ������������)
	void add_option(inquiry &aInquiry, String aOption);	//����� ������ ������ inquiry
	client operator+ (String aOption);		//���������� add_option, �� ����� ������������� ��������
	client operator+= (String aOption);		//���������� add_option, �� ����� ������������� ��������
	void change_inquiry(inquiry &aInquiry, int car_ID);	//�������� ���������� � �������
	void delete_option(inquiry &aInquiry, String aOption);	//������� ��������� ����� �� ����������� �������
	client operator- (String aOption);			//���������� delete_option, �� ����� ������������� ��������
	client operator-= (String aOption);			//���������� delete_option, �� ����� ������������� ��������
	void delete_inquiry(inquiry &aInquiry);		//������� ���������� ������
	client operator- (inquiry aInquairy);		//���������� delete_inquiry, �� ����� ������������� ��������
	client operator-= (inquiry aInquairy);		//���������� delete_inquiry, �� ����� ������������� ��������
	bool operator== (const client &aClient)const;	//���������� ���� �������� �� ID � ������� 
};

//global overload of outstream
ostream& operator << (ostream& os, client aClientg);
*/
