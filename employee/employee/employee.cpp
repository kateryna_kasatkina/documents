#include "employee.h"
employee::employee()
{
	ID[0] = 0;
	ID[1] = 0;
	String a = "no data";//I am going to change it when we apply String, because it couse a problem
	name = a;
	surname = a;
	phone = a;
	birth.day = 0;
	birth.month = 0;
	birth.year = 0;
	type = mgr;
}
employee::employee(String aSurname, String aName, int IDsalon, int IDemployee, int aDay, int aMonth, int aYear, String aPhone, position aType)
{
	ID[0] = IDsalon;
	ID[1] = IDemployee;
	name = aName;
	surname = aSurname;
	phone = aPhone;
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
	type = aType;

}
employee::employee(const employee &a)
{
	ID[0] = a.ID[0];
	ID[1] = a.ID[1];
	name = a.name;
	surname = a.surname;
	phone = a.phone;
	birth.day = a.birth.day;
	birth.month = a.birth.month;
	birth.year = a.birth.year;
	type = a.type;
}
void employee::set_name(String aName)
{
	name = aName;
}
void employee::set_surname(String aSurname)
{
	surname = aSurname;
}
void employee::set_phone(String aPhone)
{
	phone = aPhone;
}
void employee::set_ID_employee(int IDemployee)
{
	ID[1] = IDemployee;

}
void employee::set_ID_salon(int IDsalon)
{
	ID[0] = IDsalon;
}
void employee::set_position(position aType)
{
	type = aType;
}
void employee::set_birth_date(int aDay, int aMonth, int aYear)
{
	birth.day = aDay;
	birth.month = aMonth;
	birth.year = aYear;
}
void employee::show_employee()
{
	cout << "ID: " << ID[0] << "-" << ID[1] << endl;
	cout << "Name: " << name << endl;
	cout << "Surname: " << surname << endl;
	cout << "Phone: " << phone << endl;
	cout << "Date of birth: " << birth.day << "." << birth.month << "." << birth.year << endl;
	if (type == mgr)
		cout << "manager" << endl;
	else
		cout << "director" << endl;

}

int employee::get_salon_ID()
{
	cout << "salon_ID: " << ID[0] << endl;
	return ID[0];
}
int employee::get_employee_ID()
{
	cout << "employee_ID: " << ID[1] << endl;
	return ID[1];
}
position employee::get_position()const
{
	if (type == mgr)
		cout << "manager" << endl;
	else
		cout << "director" << endl;
	return type;
}

ostream& operator<<(ostream& os, const employee& dt)
{
	os << "ID: " << dt.ID[0] << "-" << dt.ID[1] << endl;
	os << "Name: " << dt.name << endl;
	os << "Surname: " << dt.surname << endl;
	os << "Phone: " << dt.phone << endl;
	os << "position: " << endl;
	if (dt.type == mgr)
		cout << "manager" << endl;
	else
		cout << "director" << endl;
	return os;
}