#pragma once
#include <iostream>
#include "employee.h"
#include "inquiry.h"
using std::endl;
using std::ostream;
 class inquiry;
class employee;
class date
{
	friend class inquiry;
	friend class employee;
private:
	int day;
	int month;
	int year;

	//friend class Person;
	
	//friend ostream & operator << (ostream& os, const String &astring); //����� ������ String - ����� � ����� ������ String
	//friend ostream & operator << (ostream& os, const Person &aperson); //����� ������ Person - ����� � ����� ������ Person
																	   //��� ������ ���� �����������: friend ostream& operator << (ostream& os, date &aDate) - ����
	friend ostream& operator << (ostream& os, date &aDate);
public:
	date()	//default constructor
	{
		day = 0;
		month = 0;
		year = 0;
	}

	date(int aDay, int aMonth, int aYear)	//overloaded constructor
	{
		day = aDay;
		month = aMonth;
		year = aYear;
	}

	date(const date &BD)	//copy constructor
	{
		day = BD.day;
		month = BD.month;
		year = BD.year;
	}

	~date() {}		//destructor

					// ���������� ��������� ������������
					// ��������� ������ �� ��������� ������ date
					// ���������� ������� ��������� ������ date
	date& operator= (const date &aDate)
	{
		day = aDate.day;
		month = aDate.month;
		year = aDate.year;
		return *this;
	}

	// ���������� ��������� ������
	// ��������� ������ �� ��������� ������ date � ostream
	// ���������� ��������� ������ ostream
	
};

