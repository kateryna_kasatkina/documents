#pragma once
#include <iostream>
#include <string>
using namespace std;
class word
{
	string word_english;//word in english
	string word_russian;//in russian
	int counter;//counter of popularity
	word *left;//pointer for left node
	word *right;//pointer for right node
	word *parent;//pointer for parent
public:
	word()//default constructor
	{
		string a = "no data";
		word_english = a;
		word_russian = a;
		counter = 0;
		left=nullptr;
		right=nullptr;
		parent = nullptr;
	}
	word(string a_ing)//overloaded constructor, use to create word wich will be translated
	{
		string a = "no data";
		word_english = a_ing;
		word_russian = a;
		counter = 0;
		left = nullptr;
		right = nullptr;
		parent = nullptr;
	}
	word(string a_ing,string a_rus)//overloaded constructor
	{
		word_english = a_ing;
		word_russian = a_rus;
		counter = 0;
		left=nullptr;
		right = nullptr;
		parent = nullptr;
	}
	word(string a_ing, string a_rus,int aCounter)//overloaded constructor
	{
		word_english = a_ing;
		word_russian = a_rus;
		counter = aCounter;
		left = nullptr;
		right = nullptr;
		parent = nullptr;
	}
	//methods to set and get values or pointers
	void set_parent(word *aParent)
	{
		parent = aParent;
	}
	word *get_parent()
	{
		return parent;
	}
	void set_left(word *aLeft)
	{
		left = aLeft;
	}
	word *get_left()
	{
		return left;
	}
	word *get_right()
	{
		return right;
	}
	void set_right(word *aRight)
	{
		right = aRight;
	}
	void set_eng(string a_ing)
	{
		word_english = a_ing;
	}
	void set_rus(string a_rus)
	{
		word_russian = a_rus;
	}
	void set_counter(int aCounter)
	{
		counter = aCounter;
	}
	void increase_counter()
	{
		counter++;
	}
	string get_eng()
	{
		return word_english;
	}
	string get_rus()
	{
		return word_russian;
	}
	int get_counter()
	{
		return counter;
	}
	//overloaded operators to compare words
	bool operator>(word *a)
	{
		if (counter > a->get_counter())
			return true;
		else
			return false;
	}
	bool operator<(word *a)
	{
		if (counter < a->get_counter())
			return true;
		else
			return false;
	}
};

