#pragma once
#include <string>
#include <iomanip>
#include "word.h"

using namespace std;
class tree
{
	friend ostream& operator<<(ostream& os, word &a);//friendly overloaded cout(prototype)
	word *root;
public:
	
	tree()//default constructor
	{
		root = nullptr;
	}
	void print()//method passes root to print_helper for printig tree
	{
		print_helper(root);
	}
	int print_helper(word * el)//prints tree min-max
	{
	
		if (el->get_left())
			print_helper(el->get_left());
		cout << (*el);
        if (el->get_right())
			print_helper(el->get_right());
		
		return 0;
	}

	void minTree()//method passes root to minTree_helper for searching minimum of tree
	{
		minTree_helper(root);
	}
	
	word *minTree_helper(word *rt)//search minimum of tree
	{
		if (rt == NULL) return NULL;
		while (rt->get_left())
			rt = rt->get_left();
		cout <<"Minimum: "<<(*rt);
		return rt;
	}
	void show_top_down()//shows three most unpopular words
	{
		word *tmp = minTree_helper(root);//print the smallest
		tmp = tmp->get_parent();
		cout << (*tmp);//parent of the smallest 
		if (tmp->get_right()->get_left())
		{
			tmp = tmp->get_right()->get_left();//if parent of the smollest has child
			cout << (*tmp);//prints it
		}
		else
		{
			tmp = tmp->get_right();
			cout << (*tmp);
		}
	}


	void maxTree()//method passes root to maxTree_helper for searching maximum of tree
	{
		maxTree_helper(root);
	}
	word *maxTree_helper(word *rt)//search maximum of tree
	{
		while (rt->get_right())
			rt = rt->get_right();
		cout << "Maximum: " << (*rt);
		return rt;
	}
	void show_top_up()//shows three most popular words
	{
		word *tmp = maxTree_helper(root);//print the biggest
		if (tmp->get_left()->get_right())
		{
			tmp = tmp->get_left()->get_right();
			cout << (*tmp);
		}
		if (tmp->get_parent() > tmp->get_left())
		{
			tmp = tmp->get_parent();
			cout <<(* tmp);
		}
		else
		{
			tmp = tmp->get_left();
			cout << (*tmp);
		}


	}
	
	void look_up(string key)//passes root to look_up_helper
	{
		look_up_helper(root, key);
	}
	word *look_up_helper( word *rt ,string key)//search translation in tree
	{
		if (rt == nullptr)
			return rt;
		if (rt->get_eng() == key)
		{
			rt->increase_counter();
			cout <<(*rt);
			return rt;
		}
		else
		{
			if (rt->get_eng() < key)
				return look_up_helper(rt->get_right(), key);
			else
				return look_up_helper(rt->get_left(), key);
		}
		return 0;
	}
	// adding word to tree
	void add(string a_eng,string a_rus,int a_counter) {
		if (!root)
			root = new word(a_eng,a_rus,a_counter);
		else
		{
			word *tmp = root;
			while (tmp)
			{
				if (a_counter< tmp->get_counter())
					if (tmp->get_left() == NULL)
					{
						tmp->set_left(new word(a_eng, a_rus, a_counter));
						tmp->get_left()->set_parent(tmp);
						break;
					}
					else tmp = tmp->get_left();
				else {
					if (a_counter>tmp->get_counter())
						if (tmp->get_right() == NULL)
						{
							tmp->set_right(new word(a_eng, a_rus, a_counter));
							tmp->get_right()->set_parent(tmp);
							break;
						}
						else tmp = tmp->get_right();
					else tmp = NULL;
				}
			}
		}
	}
	
};

ostream& operator<<(ostream& os, word &a)//friendly overloaded cout
{
	os << a.get_eng() << "-" << a.get_rus() << "  counter: " << a.get_counter() << endl;
	return os;
}