#pragma once
class Element
{
	int value;
	Element *next;
	Element *prev;
public:
	Element()
	{
		value = 0;
		next = prev = nullptr;
	}
	Element(int aValue)
	{
		value = aValue;
		next = prev = nullptr;
	}
	void set_value(int aValue)
	{
		value = aValue;
	}
	int get_value()
	{
		return value;
	}
	void set_next(Element *aNext)
	{
		next = aNext;
	}
	Element *get_next()
	{
		return next;
	}
	void set_prev(Element *aPrev)
	{
		prev = aPrev;
	}
	Element *get_prev()
	{
		return prev;
	}
};