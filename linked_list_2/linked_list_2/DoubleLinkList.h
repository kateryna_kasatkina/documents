#pragma once
#include"Element.h"
using namespace std;
class dl
{
	Element *head;
	Element *tail;
	
public:
	dl()
	{
		head = tail = nullptr;
		
	}
	~dl()
	{
		while (head)
		{
			Element *tmp_el = head;
			head = head->get_next();
			delete tmp_el;
		}
	}
	void add(int aValue)
	{
		Element *tmp_el = new Element(aValue);
	
		if (head)
		{
			tail->set_next(tmp_el);
			tmp_el->set_prev(tail);
		}
		else
			head = tmp_el;
		tail = tmp_el;
	}
	void show()
	{
		
		Element *tmp_el = head;
		while (tmp_el)
		{
			cout << tmp_el->get_value()<< "\n";
			tmp_el = tmp_el->get_next();
		}
	}
	void show_reverse()
	{
		Element *tmp_el = tail;
		while (tmp_el)
		{
			cout << tmp_el->get_value() << "\n";
			tmp_el = tmp_el->get_prev();
		}
	}
	bool del_by_index(unsigned int index)
	{
		if (index)//���� ��� �������� ������!=0, ������ ��� �� �������������
		{
			Element *prev = head;//��� ������ ���������� ����� ������
			for (int i = 0; i < index - 1 && prev; i++)
				prev = prev->get_next();//������� ������� �������������� �������

			Element* tmp = nullptr;
			if (prev)
				tmp = prev->get_next();
			else
				tmp = NULL;


			if (tmp)//���� �� ����� �� ������ ������� ��� ��������
			{
				prev->set_next(tmp->get_next());
				if (tmp->get_next())
					tmp->get_next()->set_prev(prev);
				if (tmp == tail)
					tail = prev;
				delete tmp;
			
			}
		}
		else
		{
			Element *tmp = head;
			head = head->get_next();
			delete tmp;
			
		}
		return true;
	}
	bool insertByIndex(int index, int value) {
		if (index > 0) {

			Element *prev = head;
			for (int i = 0; i<index - 1 && prev; i++)
				prev = prev->get_next();

			if (prev) {
				Element* insElem = new Element(value);
			
				insElem->set_next(prev->get_next());
				if (prev->get_next())
					prev->get_next()->set_prev(insElem);

				insElem->set_prev(prev);
				prev->set_next(insElem);

				if (prev == tail)  tail = insElem;
			}
			else return false;
		}
		else {
			Element* insElem = new Element(value);
			
			insElem->set_next(head);
			head = insElem;
		}
		return true;
	}

};