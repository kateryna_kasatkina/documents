#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iterator>
#include <algorithm>
#include "time_table.h"
using std::cout;
using std::endl;
using std::string;
using std::pair;
using std::vector;
enum class state { free, booked, occupaid };
typedef pair<int, string> my_pair;
class table 
{
	friend ostream& operator<<(ostream& os, table&a);
protected:
	int number_table=0;
	int number_of_chairs=0;
	map <int, string> time_table;
public://restauran works from 10.00 to 23.00
	table()
	{
		number_table = 0;
		number_of_chairs = 0;
		time_table.insert(my_pair(10, "free"));
		time_table.insert(my_pair(11, "free"));
		time_table.insert(my_pair(12, "free"));
		time_table.insert(my_pair(13, "free"));
		time_table.insert(my_pair(14, "free"));
		time_table.insert(my_pair(15, "free"));
		time_table.insert(my_pair(16, "free"));
		time_table.insert(my_pair(17, "free"));
		time_table.insert(my_pair(18, "free"));
		time_table.insert(my_pair(19, "free"));
		time_table.insert(my_pair(20, "free"));
		time_table.insert(my_pair(21, "free"));
		time_table.insert(my_pair(22, "free"));
		time_table.insert(my_pair(23, "free"));
	}

	table(int aNumber_table, int aNumber_of_chairs)
	{
		number_table = aNumber_table;
		number_of_chairs = aNumber_of_chairs;
		time_table.insert(my_pair(10, "free"));
		time_table.insert(my_pair(11, "free"));
		time_table.insert(my_pair(12, "free"));
		time_table.insert(my_pair(13, "free"));
		time_table.insert(my_pair(14, "free"));
		time_table.insert(my_pair(15, "free"));
		time_table.insert(my_pair(16, "free"));
		time_table.insert(my_pair(17, "free"));
		time_table.insert(my_pair(18, "free"));
		time_table.insert(my_pair(19, "free"));
		time_table.insert(my_pair(20, "free"));
		time_table.insert(my_pair(21, "free"));
		time_table.insert(my_pair(22, "free"));
		time_table.insert(my_pair(23, "free"));

	}
	table(const table &a)
	{
		number_table = a.number_table;
		number_of_chairs = a.number_of_chairs;
		time_table = a.time_table;
	}
	table& operator=(const table &a)
	{
		number_table = a.number_table;
		number_of_chairs = a.number_of_chairs;
		time_table = a.time_table;
		return *this;
	}
	void show_time_table()
	{
		map <int, string>::iterator begin_pIter, end_pIter;
		for (begin_pIter = time_table.begin(); begin_pIter != time_table.end(); begin_pIter++)
			cout << " " << begin_pIter->first << "(" << begin_pIter->second << ")";
		cout << "." << endl;
	}
	void set_number_table(int aNumber_table)
	{
		number_table = aNumber_table;
	}
	void set_number_of_chairs(int aNumber_of_chairs)
	{
		number_of_chairs = aNumber_of_chairs;
	}
	int get_number_table()
	{
		return number_table;
	}
	int get_number_of_chairs()
	{
		return number_of_chairs;
	}
	void make_table_booked(int aHour)
	{
		auto result = time_table.find(aHour)->second = "booked";
	}
	void make_table_occupaid(int aHour)
	{
		auto result = time_table.find(aHour)->second = "occupaid";
	}
	void make_table_free(int aHour)
	{
		auto result = time_table.find(aHour)->second = "free";
	}

	void show_free_hours()
	{
		string target = "free";
		map <int, string>::iterator begin_pIter, end_pIter;
		for (begin_pIter = time_table.begin(); begin_pIter != time_table.end(); begin_pIter++)
		{
			if (begin_pIter->second == "free")
				cout << " " << begin_pIter->first << "(" << begin_pIter->second << ")";
		}
		cout << "." << endl;

	}
	void write_to_file_table(fstream &f)
	{
		string x = "Number of table: ";
		size_t size_x = x.size();
		f.write(reinterpret_cast<char*>(&size_x), sizeof(size_x));
		f.write(x.c_str(), size_x);

		f.write(reinterpret_cast<char*>(&number_table), sizeof(number_table));

		for (auto& x : time_table) {
			int w = x.first;
			f.write(reinterpret_cast<char*>(&w), sizeof(w));
			size_t size_s = (x.second).size();
			f.write(reinterpret_cast<char*>(&size_s), sizeof(size_s));
			f.write((x.second).c_str(), size_s);
		}

	}
};

ostream& operator<<(ostream& os, table&a)
{
	os << "Number of table: " << a.get_number_table() << endl;
	os << "Number of chairs: " << a.get_number_of_chairs() << endl;
	map <int, string>::iterator begin_pIter, end_pIter;
	for (begin_pIter = a.time_table.begin(); begin_pIter != a.time_table.end(); begin_pIter++)
		os << " " << begin_pIter->first << "(" << begin_pIter->second << ")";
	os << "." << endl;
	return os;
}