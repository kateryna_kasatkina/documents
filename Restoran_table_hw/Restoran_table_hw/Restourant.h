#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include "manager.h"
#include "Table.h"
#include <exception>
using namespace std;
class Restourant :public manager
{
	friend ostream& operator<<(ostream& os, Restourant&a);
public:
	int number_of_tables;
	vector<table> all_tables;
	manager main_manager;

	Restourant(string aName, string aSurname) : main_manager(aName, aSurname), number_of_tables(0) {}
	void add_table_to_vector(int aNumber_table, int aNumber_of_chairs)
	{
		number_of_tables++;
		table t(aNumber_table, aNumber_of_chairs);
		all_tables.push_back(t);

	}
	void delete_table_by_number(int number)
	{
		auto new_tables = remove_if(begin(all_tables), end(all_tables), [&number](table &a) {return a.get_number_table() == number; });
		all_tables.erase(new_tables, end(all_tables));
		number_of_tables--;
	}
	int make_table_occupaid_by_number(int number, int aHour, int number_of_hours)
	{
		if (aHour < 10 || aHour + number_of_hours>23)
		{
			cout << "Restaurant does not work at this time ask manager for advice" << endl;
			return -1;
		}
		auto ptr = find_if(begin(all_tables), end(all_tables), [&number](table &a) {return a.get_number_table() == number; });
		for (int i = 0; i < number_of_hours; i++)
		{
			ptr->make_table_occupaid(aHour);
			aHour++;
		}
		return 0;
	}
	int make_table_booked_by_number(int number, int aHour, int number_of_hours)
	{
		if (aHour < 10 || aHour + number_of_hours>23)
		{
			cout << "Restaurant does not work at this time ask manager for advice" << endl;
			return -1;
		}
		auto ptr = find_if(begin(all_tables), end(all_tables), [&number](table &a) {return a.get_number_table() == number; });
		for (int i = 0; i < number_of_hours; i++)
		{
			ptr->make_table_booked(aHour);
			aHour++;
		}
		return 0;
	}
	int make_table_free_by_number(int number, int aHour, int number_of_hours)
	{
		if (aHour < 10 || aHour + number_of_hours>23)
		{
			cout << "Restaurant does not work at this time ask manager for advice" << endl;
			return -1;
		}
		auto ptr = find_if(begin(all_tables), end(all_tables), [&number](table &a) {return a.get_number_table() == number; });
		for (int i = 0; i < number_of_hours; i++)
		{
			ptr->make_table_free(aHour);
			aHour++;
		}
		return 0;
	}

	void write_to_file(fstream &f)
	{

		f.write(reinterpret_cast<char*>(&number_of_tables), sizeof(number_of_tables));
		string n = main_manager.get_name_manager();
		size_t size_n = n.size();
		f.write(reinterpret_cast<char*>(&size_n), sizeof(size_n));
		f.write(n.c_str(), size_n);

		string sn = main_manager.get_surname_manager();
		size_t size_sn = sn.size();
		f.write(reinterpret_cast<char*>(&size_sn), sizeof(size_sn));
		f.write(sn.c_str(), size_sn);

		for (auto& x : all_tables)
		{
			x.write_to_file_table(f);
		}
	}
};
	ostream& operator<<(ostream& os, Restourant&a)
	{
		os << "quantity of tables: " << a.number_of_tables << endl;
		os << a.main_manager;
		for (size_t i = 0; i < a.all_tables.size(); i++)
		{
			cout << a.all_tables[i] << endl;
		}
		return os;
	}

