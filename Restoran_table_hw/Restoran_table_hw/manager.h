#pragma once
#include <iostream>
#include <string>
#include "Table.h"
using namespace std;
class manager :public  table
{
	friend ostream& operator<<(ostream& os, manager&a);
protected:
	string name_manager;
	string surname_manager;
public:
	manager()
	{
		name_manager = "";
		surname_manager = "";
	}
	manager(string aName, string aSurname)
	{
		name_manager = aName;
		surname_manager = aSurname;
	}
	void set_name_manager(string aName)
	{
		name_manager = aName;
	}
	string get_name_manager()
	{
		return name_manager;
	}
	string get_surname_manager()
	{
		return surname_manager;
	}
	void set_surname_manager(string aSurname)
	{
		surname_manager = aSurname;
	}
	//methods to make table booked, occupaid or free only for hour
	void make_table_booked(table a, int aHour)
	{
		a.make_table_booked(aHour);
	}
	void make_table_occupaid(table a, int aHour)
	{
		a.make_table_occupaid(aHour);
	}
	void make_table_free(table a, int aHour)
	{
		a.make_table_free(aHour);
	}

};
ostream& operator<<(ostream& os, manager&a)
{
	os << "Manager: " << a.name_manager << " " << a.surname_manager << endl;
	return os;

}