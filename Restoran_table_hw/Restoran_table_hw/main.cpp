/*���������� ����������� ������� ������������ �������� � ���������.

������ ������ ����� ������������ ���-�� ���������� ���� � ����� � �������� � �� 
������� �� �����.

� ��������� ���� ����������� ������ ������ �� ��������� �����.

���� ������ �������� �� ������� ������ ������������� ����� 20 ����� � ���������� 
�������� ��� ������ � �������.

������ � ������� ����������� ��������� �������. ��� �������� �����������������. 

��� ���������� ������ ������������  � �������� ���� ��� ������ �������. ��� ����������
����� ������ �������������� ����������.

��� �������������� ������ ���� ����������� � ����� ���������� �������������� �++.
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include "Table.h"
#include "manager.h"
#include "Restourant.h"
#include <exception>
using std::cout;
using std::endl;
int main()
{
	
	Restourant fun ("first", "manager");
	fun.add_table_to_vector(1, 5);
	fun.add_table_to_vector(2, 5);
	fun.add_table_to_vector(3, 5);
	fun.add_table_to_vector(4, 2);
	fun.add_table_to_vector(5, 2);
	fun.add_table_to_vector(6, 4);
	fun.delete_table_by_number(2);
	fun.make_table_occupaid_by_number(1, 11, 4);
	fun.make_table_booked_by_number(6, 14, 4);
	cout << fun;
	cout << "error---------------------------------\n";
	//test with wrong time period
	fun.make_table_free_by_number(6, 16,12);//(int number, int aHour, int number_of_hours)
	

	fstream f;
	try 
	{
		f.open("restourant.bin", ios::binary| ios::app);
		fun.write_to_file(f);
	}
	catch (exception& e)
	{
		cout << "Standard exception: " << e.what() << endl;
	}

	f.close();
}