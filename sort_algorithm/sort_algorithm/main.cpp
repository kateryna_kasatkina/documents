#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <iterator>
using namespace std;

class person {
	friend ostream &operator<<(ostream &os, person&a);
	string name;
	string surname;
public:
	person()
	{
		name="";
		surname="";
	}
	person(string aName, string aSurname)
	{
		name = aName;
		surname = aSurname;
	}
	person(const person &a)
	{
		name = a.name;
		surname = a.surname;
	}
	string get_name()
	{
		return name;
	}
	string get_surname()
	{
		return surname;
	}
	bool operator>(const person&a)
	{
		if (surname > a.surname) 
			return true;
		if ((surname == a.surname) && (name > a.name))
			return true;
			return false;
	}
	bool operator<(const person&a)
	{
		if (surname < a.surname)
			return true;
		if ((surname == a.surname) && (name < a.name))
			return true;
		return false;
	}
	person operator=(const person&a)
	{
		name = a.name;
		surname = a.surname;
		return *this;
	}
	
};
ostream &operator<<(ostream &os, person&a)
{
	os << "Name: " << a.name << endl;
	os << "Surname: " << a.surname << endl;
	return os;
}

bool ByFirstname(person &p1, person &p2) {
	return p1.get_name() < p2.get_name();
}
bool ByLastname(person &p1, person &p2) {
	return p1.get_surname() < p2.get_surname();
}
int main()
{
	person x1("Alex", "Ivanov");
	person x2("Boris", "Ivanov");
	person x3("Alex", "Sidorov");
	person x4("Boris", "Sidorov");
	vector<person> v;
	vector<person> ::iterator Iter1;
	v.push_back(x1);
	v.push_back(x2);
	v.push_back(x3);
	v.push_back(x4);

	cout << "Original vector v = ( ";
	for (Iter1 = v.begin(); Iter1 != v.end(); Iter1++)
		cout << *Iter1 << " ";
	cout << ")" << endl;

	/*cout << "=======================\n";
	sort(v.begin(), v.end());
	cout << "Sorted vector v1 = ( ";
	for (Iter1 = v.begin(); Iter1 != v.end(); Iter1++)
		cout << *Iter1 << " ";
	cout << ")" << endl;*/

	// A user-defined (UD) binary predicate can also be used  
	sort(v.begin(), v.end(), ByFirstname);
	cout << "Resorted ByFirstname :";
	for (Iter1 = v.begin(); Iter1 != v.end(); Iter1++)
		cout << *Iter1 << " ";
	cout <<  endl;

	cout << "=======================\n";

	// A user-defined (UD) binary predicate can also be used  
	sort(v.begin(), v.end(), ByLastname);
	cout << "Resorted ByLastname:";
	for (Iter1 = v.begin(); Iter1 != v.end(); Iter1++)
		cout << *Iter1 << " ";
	cout << endl;
}