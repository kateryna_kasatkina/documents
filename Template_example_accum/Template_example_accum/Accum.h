#pragma once
template <class T>
class Accum
{
private:
	T total;
public:
	//Accum(T start) :total(start) {};
	Accum()
	{
		total = 0;
	}
	Accum(T start)
	{
		total = start;
	}
	T operator+=(const T&t)
	{
		return total = total + t;
	}
	T get_total()
	{
		return total;
	}
};