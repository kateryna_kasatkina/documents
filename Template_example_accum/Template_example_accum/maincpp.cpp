#include <iostream>
#include <string>
#include "Accum.h"
using namespace std;
int main()
{
	Accum<int> integers(0);
	integers += 3;
	integers += 7;
	cout << integers.get_total() << endl;

	Accum<string> st("");
	st += "hello";
	st += " world";
	cout << st.get_total() << endl;
}