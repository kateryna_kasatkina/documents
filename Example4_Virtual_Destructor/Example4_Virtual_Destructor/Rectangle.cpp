#include "Rectangle.h"
#include <iostream>
using namespace std;

Rectangle::Rectangle()
{
	cout << "Rectangle::Rectangle()" << endl<< endl;
}

Rectangle::~Rectangle()
{
	cout << "Rectangle::~Rectangle()" << endl;
}

double Rectangle::square()
{
	cout << "Rectangle::square()"<<endl;
	return 0.0;
}
