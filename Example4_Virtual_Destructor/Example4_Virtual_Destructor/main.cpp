#include <iostream>
using namespace std;
#include "Object.h"
#include "Ellipse.h"
#include "Rectangle.h"
void main()
{


	Object* ob_arr[3];

	ob_arr[0] = new Object;
	cout << endl;

	ob_arr[1] = new Ellipse;
	ob_arr[2] = new Rectangle;
	cout << endl;


    for(int i=0; i<3; i++)
		ob_arr[i]->square();

	cout << endl;

	for(int i=0; i<3; i++)
		delete ob_arr[i];

}