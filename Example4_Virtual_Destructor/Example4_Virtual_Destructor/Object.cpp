#include "Object.h"
#include <iostream>
using namespace std;

Object::Object(){
	cout << "Object::Object()"<<endl;
}

double Object::square()
{
	cout << "Object::Square()"<<endl;
	return 0.0;
}

Object::~Object()
{
	cout << "Object::~Object()" << endl  << endl;
}