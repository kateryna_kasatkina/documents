#pragma once
#include "String.h"
#include "inquiry.h"

/*������ ������� (��� ������������� � �� �������������):
added - ����� ������, ��� �� ���������������
changed - ������ ������, �� ������ ���� �������
synchronyzed - ������ ������, ������ �� ���������� � ������� ��������� �������������
*/
enum client_status { added, changed, synchronyzed };


/*������������ � �������  manufacturerDB � salonDB, ������ ���������� � �������� � ��� ������� */
class client
{
private:
	int ID[2];					//ID ������ � ID �������
	client_status status;		//������ ������� (��.����)
	String name;
	String surname;
	String phone;
	int list_sz;				//���������� �������� � ������� �������
	inquiry *inquirys_list;		//������ �������� ������� �������

	//friend class DataBase;		//�.�. ���������� ������ DataBase �������� � ������������ ����� ������????

public:
	client()//default constructor
	{
		ID[0] = 0;
		ID[1] = 1;
		status = added;
		name = nullptr;
		surname = nullptr;
		phone = nullptr;
		list_sz = 0;
		inquirys_list = nullptr;
	}
	client(int aSalon_ID, int aClient_ID, String aName, String aSurname, String aPhone)//overloaded constructor
	{
		ID[0] = aSalon_ID;
		ID[1] = aClient_ID;
		status = added;
		name = aName;
		surname = aSurname;
		phone = aPhone;
		list_sz = 0;
		inquirys_list = nullptr;
	}
	client(const client &aClient)//copy constructor
	{
		ID[0] = aClient.ID[0];
		ID[1] = aClient.ID[1];
		status = aClient.status;
		name = aClient.name;
		surname = aClient.surname;
		phone = aClient.phone;
		list_sz = aClient.list_sz;
		inquirys_list = new inquiry[aClient.list_sz];
		for (int i = 0; i < aClient.list_sz; i++)
		{
			inquirys_list[i] = aClient.inquirys_list[i];
		}
	}
	~client()//destructor
	{
		delete[]inquirys_list;
	}

public:
	//inline �� ���� ���������� � ���� ����������, ��� ����� - ������ � ����������!
	int get_salon_ID()
	{
		cout << "Salon_ID: " << ID[0] << endl;
		return  ID[0];
	}
	int get_client_ID()
	{
		cout << "Client_ID: " << ID[1] << endl;
		return  ID[1];
	}
	client_status get_status()
	{
		if (status == 0)
			cout << "Status: added" << endl;
		if (status == 1)
			cout << "Status: changed" << endl;
		if (status == 2)
			cout << "Status: synchronyzed" << endl;
		return status;
	}
	int get_list_sz()//���������� ���������� �������� � ����� �������
	{
		cout << "Number of iquiries: " << list_sz << endl;
		return list_sz;
	}
	inquiry *get_inquiry_list()const//���������� ��������� �� ������ �������
	{
		return inquirys_list;
	}

	/*set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
	void set_ID(int aSalon_ID, int aClient_ID)
	{
		ID[0] = aSalon_ID;
		ID[1] = aClient_ID;
	}

	void set_salon_ID(int aSalon_ID)
	{
		ID[0] = aSalon_ID;
	}
	void set_client_ID(int aClient_ID)
	{
		ID[1] = aClient_ID;
	}

	void set_client(String aSurname, String aName, String aPhone = nullptr)
	{
		name = aName;
		surname = aSurname;
		phone = aPhone;

	}

	void set_client_status(client_status aStstus)
	{
		status = aStstus;
	}

	//�������� (������ ������), ���� ����� ����� - ���� ��������
											/*�������� ������ �� ������, �������� ��� ID: ID ������(1-� ������� ID �������)
											- ID ���������=0(����� ��������� ����� ������� ������ ��������) -
											ID ������� (2-� ������� ID �������) �
											ID ���� (�� ��������� ������) -
											ID ������ (���������� ����� � ������ �������� ����� �������)

	*/
	///////////////////////////////////////////////////////////////////
	/*enum  index_ID //��� �������� ������� //���, � �����? ��� �������������? ��� ����� ��������� ����� �� ������� �������? ����� �� ���������� ������ ID[0], ID[1].. ������ �� �������, ��� ���������������� ��� ������ ����� ��������� ������������ ������ int. �� �����������? ����������?
	{
		iSalonID = 0,
		iManagerID = 1,
		iClientID = 2,
		iCarID = 3,
		iInquiryID = 4
	};
	int ID[5];					//ID �������ID ���������- ID �������� ID ���� - ID ������
	date date_added;
	/*� ��������, ��, ��������. �����, ID ���������� � ������ ����� ����������, ����� ������� ��� ��������*/
	//String model;				//������ ������ ������������� �� �������� ����������� �� ID ���� - ����� ������������ ������
	//DataBase *DB              // ��� �������� ��� ���� � ������� ��������� �������� ������ DataBase, ����� �������� ������ ������
	/*inquiry_status status;		//������ �������  (��.����)
	int options_sz;				//���������� ��������� ��� �����
	String *additional_options;	//������ ���.�����, ��������� ��������
	*/
	/*void set_salon_ID(int aSalonID);
	void set_client_ID(int aClientID);
	void set_manager_ID(int aManagerID);
	void set_car_ID(int aCarID);
	void set_inquiry_ID(int aInquiryID);
	void set_data(int aDay, int aMonth, int aYear);
	void set_data(date & aDate);
	void set_status(inquiry_status aStatus);	//�������� ������*/
	//////////////////////////////////////////////////////////////////
	
	inquiry fill_in_iquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aInquiryID, int aDay, int aMonth, int aYear, inquiry_status aStatus)
	{
		inquiry x;
		x.set_salon_ID(aSalonID);
		x.set_client_ID(aClientID);
		x.set_manager_ID(aManagerID);
		x.set_car_ID(aCarID);
		x.set_inquiry_ID(aInquiryID);
		x.set_data(aDay, aMonth, aYear);
		x.set_status(aStatus);
		return x;
	}
	void add_inquiry(int aSalonID, int aManagerID, int aClientID, int aCarID, int aInquiryID, int aDay, int aMonth, int aYear, inquiry_status aStatus)
	{
		inquiry *inquirys_list_new = new inquiry[list_sz + 1];
		for (int i = 0; i < list_sz+1; i++)
		{
			if (i == list_sz)
			{
				inquirys_list_new[list_sz] = fill_in_iquiry(aSalonID, aManagerID, aClientID, aCarID, aInquiryID, aDay, aMonth, aYear, aStatus);
				break;
			}
			inquirys_list_new[i] = inquirys_list_new[i];
		}
		list_sz++;
		delete[]inquirys_list;
		inquirys_list = inquirys_list_new;
	}
	void delete_inquiry(inquiry &aInquiry);		//������� ���������� ������

};
											/*client operator= (const client &aClient);
											client operator+ (inquiry aInquairy);	//�������� ������� ������ (��� ������������)
											client operator+=(inquiry aInquairy);	//�������� ������� ������ (��� ������������)
											void add_option(inquiry &aInquiry, String aOption);	//����� ������ ������ inquiry
											client operator+ (String aOption);		//���������� add_option, �� ����� ������������� ��������
											client operator+= (String aOption);		//���������� add_option, �� ����� ������������� ��������
											void change_inquiry(inquiry &aInquiry, int car_ID);	//�������� ���������� � �������
											void delete_option(inquiry &aInquiry, String aOption);	//������� ��������� ����� �� ����������� �������
											client operator- (String aOption);			//���������� delete_option, �� ����� ������������� ��������
											client operator-= (String aOption);			//���������� delete_option, �� ����� ������������� ��������
											void delete_inquiry(inquiry &aInquiry);		//������� ���������� ������
											client operator- (inquiry aInquairy);		//���������� delete_inquiry, �� ����� ������������� ��������
											client operator-= (inquiry aInquairy);		//���������� delete_inquiry, �� ����� ������������� ��������
											bool operator== (const client &aClient)const;	//���������� ���� �������� �� ID � �������
										

										//global overload of outstream
										ostream& operator << (ostream& os, client aClientg);
										};*/
