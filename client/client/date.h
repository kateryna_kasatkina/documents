#pragma once
#include <iostream>
using std::endl;
using std::ostream;
//#include "Person.h"
//#include "employee.h"

class date
{
private:
	short day;
	short month;
	short year;

	friend class Person;
	friend class inquiry;
	friend class employee;
	friend ostream & operator << (ostream& os, const String &astring); //����� ������ String - ����� � ����� ������ String
	friend ostream & operator << (ostream& os, const Person &aperson); //����� ������ Person - ����� � ����� ������ Person
																	   //��� ������ ���� �����������: friend ostream& operator << (ostream& os, date &aDate) - ����

public:
	date()	//default constructor
	{
		day = 0;
		month = 0;
		year = 0;
	}

	date(short aDay, short aMonth, short aYear)	//overloaded constructor
	{
		day = aDay;
		month = aMonth;
		year = aYear;
	}

	date(const date &BD)	//copy constructor
	{
		day = BD.day;
		month = BD.month;
		year = BD.year;
	}

	~date() {}		//destructor

					// ���������� ��������� ������������
					// ��������� ������ �� ��������� ������ date
					// ���������� ������� ��������� ������ date
	date& operator= (const date &aDate)
	{
		day = aDate.day;
		month = aDate.month;
		year = aDate.year;
		return *this;
	}

	// ���������� ��������� ������
	// ��������� ������ �� ��������� ������ date � ostream
	// ���������� ��������� ������ ostream
	friend ostream& operator << (ostream& os, date &aDate)
	{
		os << aDate.day << "." << aDate.month << "." << aDate.year << endl;
		return os;
	}
};

