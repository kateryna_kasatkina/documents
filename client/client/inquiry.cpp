#include "inquiry.h"

//����������� �� ���������
inquiry::inquiry()
{
	for (int i = 0; i < 5; i++)
	{
		ID[i] = 0;
	}
	date_added.day = 0;
	date_added.month = 0;
	date_added.year = 0;
	//model = nullptr;	//������
	status = inq_added;
	options_sz = 0;
	additional_options = nullptr;

}

//������������� ����������� - 1� �������
inquiry::inquiry
(int aSalonID,
	int aManagerID,
	int aClientID,
	int aCarID, //��� ��� ����
	int aInquiryID,
	//int car_ID, // car_ID - ������? ��, ������, ������
	int aDay,
	int aMonth,
	int aYear)
{
	ID[iSalonID] = aSalonID; //� ������ �� ������ ID[0], ID[1]... � �.�.? ���������� ������ ����� ���������������� ��� ������ ������ int? 
	ID[iManagerID] = aManagerID;
	ID[iClientID] = aClientID;
	ID[iCarID] = aCarID;
	ID[iInquiryID] = aInquiryID;
	date_added.day = aDay;
	date_added.month = aMonth;
	date_added.year = aYear;
	//model = nullptr;	//������
	status = inq_added;
	options_sz = 0;
	additional_options = nullptr;
}

//������������� ����������� - 2� �������
inquiry::inquiry(
	int aSalonID,
	int aManagerID,
	int aClientID,
	int aCarID,
	int aInquiryID,
	date aDate)
{
	ID[iSalonID] = aSalonID;
	ID[iManagerID] = aManagerID;
	ID[iClientID] = aClientID;
	ID[iCarID] = aCarID;
	ID[iInquiryID] = aInquiryID;
	date_added = aDate;
	//model = nullptr; //������
	status = inq_added;
	options_sz = 0;
	additional_options = nullptr;
}

//����������� �����������
inquiry::inquiry(const inquiry & aInquiry)
{
	for (int i = 0; i < 5; i++)
	{
		ID[i] = aInquiry.ID[i];
	}
	date_added = aInquiry.date_added;
	//model = aInquiry.model;		//������
	status = aInquiry.status;
	options_sz = aInquiry.options_sz;
	if (options_sz != 0)
	{
		additional_options = new String[options_sz];
		for (int i = 0; i < options_sz; i++)
		{
			additional_options[i] = aInquiry.additional_options[i];
		}
	}
	else
	{
		additional_options = nullptr;
	}
}

//����������
inquiry::~inquiry() // ������??? ���� ���������, ��������� �� ���������� ��� ������ ����� //���, ��� ��� ���������. �� ��������� ������ ��� ������������ ������, ����� ��� ������ ������������. ������ ������ ������� �� ����.
{
	delete[] additional_options;
}

//������� ��������� � ������������� �������� ID ������
void inquiry::set_salon_ID(int aSalonID)
{
	ID[iSalonID] = aSalonID;
}

//������� ��������� � ������������� �������� ID �������
void inquiry::set_client_ID(int aClientID)
{
	ID[iClientID] = aClientID;
}

//������� ��������� � ������������� �������� ID ���������
void inquiry::set_manager_ID(int aManagerID)
{
	ID[iManagerID] = aManagerID;
}

//������� ��������� � ������������� �������� ID ������
void inquiry::set_car_ID(int aCarID) // ��� �� ������������� ������ ���������� �� ������ DataBase, ����� ���������� ��� �����? //���, ����� ��� � ����� - ����������� ID ����������.
{
	ID[iCarID] = aCarID;
}

//������� ��������� � ������������� �������� ID �������
void inquiry::set_inquiry_ID(int aInquiryID)
{
	ID[iInquiryID] = aInquiryID;
}

//������� ��������� � ������������� �������� ��� ����
void inquiry::set_data(int aDay, int aMonth, int aYear) //����� ���������� �������� ������ date? //��� �� ������� � ��������� �������� ����������
{
	date_added.day = aDay;
	date_added.month = aMonth;
	date_added.year = aYear;
}

//������� ��������� � ������������� �������� ��� ����
void inquiry::set_data(date &aDate) //����� ���������� �������� ������ date? //�� ��� � ��������� ����� - ��� ������ ������� ���������� set_data
{
	date_added = aDate;
}

//������� ��������� � ������������� �������� ������� �������
void inquiry::set_status(inquiry_status aStatus)
{
	status = aStatus;
}

//������� ��������� ������ � ������������� �������� �������������� �����
void inquiry::add_option(const String &aOption)
{
	options_sz++;
	if (options_sz == 1)
	{
		additional_options = new String[options_sz];
		additional_options[0] = aOption;
	}
	else {
		String *buf = new String[options_sz];
		for (int i = 0; i < options_sz - 1; i++)
		{
			buf[i] = additional_options[i];
		}
		delete[]additional_options;
		additional_options = buf;
		additional_options[options_sz - 1] = aOption;
	}
}

//������������� �������� ������������
//��������� ������ �� ������ ������ inquiry
inquiry & inquiry::operator=(const inquiry & aInquiry)
{
	for (int i = 0; i < 5; i++)
	{
		ID[i] = aInquiry.ID[i];
	}
	date_added.day = aInquiry.date_added.day;
	date_added.month = aInquiry.date_added.month;
	date_added.year = aInquiry.date_added.year;
	//model = aInquiry.model;		//������
	status = aInquiry.status;
	if (options_sz != 0)
	{
		delete[] additional_options;
	}
	options_sz = aInquiry.options_sz;
	if (options_sz != 0)
	{
		additional_options = new String[options_sz];
		for (int i = 0; i < options_sz; i++)
		{
			additional_options[i] = aInquiry.additional_options[i];
		}
	}
	else
	{
		additional_options = nullptr;
	}
	return *this;
}

//������������� �������� ��������
//��������� ������ �� ������ ������ String - ������
//���������� ����� ��������� ������
inquiry inquiry::operator+(const String &aOption) const
{
	return inquiry(*this) += aOption;
}

//������������� �������� +=
//��������� ������ �� ������ ������ String - ������
//���������� ������� ��������� ������
inquiry& inquiry::operator+=(const String &aOption)
{
	add_option(aOption);
	return *this;
}

//������� ������� �������� �������������� �����
//��������� ������ �� ������ ������ String - ������
// ���������� exceptions
void inquiry::remove_option(const String &aOption)
{
	int result = -1;
	for (int i = 0; i < options_sz; i++)
	{
		if (additional_options[i] == (aOption))
		{
			result = i;
			break;
		}
	}
	if (result != -1)
	{
		remove_option(result);
	}
	else
	{
		throw "BAD OPTION";
	}
}

//������� ������� �������� �������������� �����
//��������� ������ �������������� �����
// ���������� exceptions
void inquiry::remove_option(int option_number)
{
	if (option_number >= 0 && option_number < options_sz)
	{
		options_sz--;
		if (options_sz == 0)
		{
			additional_options = nullptr;
		}
		else {
			String *buf = new String[options_sz];
			for (int i = 0; i < options_sz + 1; i++)
			{
				if (i < option_number)
				{
					buf[i] = additional_options[i];
				}
				else
				{
					buf[i] = additional_options[i + 1];
				}
			}
			delete[]additional_options;
			additional_options = buf;
		}
	}
	else
	{
		throw "BAD OPTION NUMBER";
	}
}

//������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �� ������ ������ String - ������
// ���������� ����� ������ �����  inquiry
inquiry inquiry::operator-(const String &aOption) const
{
	return inquiry(*this) -= aOption;
}

//������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �� ������ ������ String - ������
// ���������� ������� ������ �����  inquiry
inquiry& inquiry::operator-=(const String &aOption)
{
	remove_option(aOption);
	return *this;
}

//������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �������������� �����
// ���������� ����� ������ �����  inquiry
inquiry inquiry::operator-(int option_number) const
{
	return inquiry(*this) -= option_number;
}

// ������� ������� �������� �������������� ����� �� �������� ������� ������
//��������� ������ �������������� �����
// ���������� ������� ������ �����  inquiry
inquiry& inquiry::operator-=(int option_number)
{
	remove_option(option_number);
	return *this;
}

// ������� �������� true ���� ����� ������ � �������, �.�. ����� ID �����
// ������� ��������� ������ �� �������� ������ inquiry
bool inquiry::operator==(const inquiry & aInquiry) const
{
	return (ID[iCarID] == aInquiry.ID[iCarID]);
}

//������� ������� � ����� ��������� ������ Inquiry
//��������� ��������� ������� ostream � inquiry
//���������� �������� ������ ostream
ostream & operator<<(ostream & os, inquiry &aInquiry)
{
	for (int i = 0; i < 5; i++)
	{
		os << "ID Inquiry" << aInquiry.ID[i] << " ";
	}
	os << endl;
	os << "Date" << aInquiry.date_added << endl;
	//os << aInquiry.model << endl;			//������
	if (aInquiry.options_sz != 0)
	{
		for (int i = 0; i < aInquiry.options_sz; i++)
		{
			os << aInquiry.additional_options[i] << endl;
		}
	}
	else
	{
		os << "No additional options" << endl;
	}
	return os;
}
