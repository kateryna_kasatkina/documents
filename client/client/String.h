#pragma once
#include <iostream>
using std::ostream;
using std::cout;

class String
{
private:
	int sz;
	char* str;

public:
	String();
	String(char *aStr);
	String(const String &aString);
	~String();

	friend ostream & operator << (ostream& os, const String &astring);

public:
	void set_string(char *aStr);
	char *get_string()const;
	int get_stringlen()const;
	String operator=(const String &aString);
	String operator+=(const String &aString);
	String operator+=(char a);
	String operator*=(const String &aString);
	String operator*=(char *aStr);
	String operator+(const String &aString);
	String operator+(char *aStr);
	String operator*(const String &aString);
	String operator*(char *aStr);
	String operator/(const String &aString);
	String operator/(char *aStr);
	String operator/=(const String &aString);
	String operator/=(char *aStr);
	bool operator==(const String &aString);
	bool operator==(char *aStr);
	bool operator!=(const String &aString);
	bool operator!=(char *aStr);
	bool operator>(const String &aString);
	bool operator>(char *aStr);
	bool operator>=(const String &aString);
	bool operator>=(char *aStr);
	bool operator<(const String &aString);
	bool operator<(char *aStr);
	bool operator<=(const String &aString);
	bool operator<=(char *aStr);
	void print();
	char& operator[](int aIndex);
	operator int()const;
	operator double()const;
	friend ostream & operator << (ostream& os, const String &aString);
};

