#pragma once
#include <string>
using namespace std;
class Element//class element for saving data
{
	friend ostream& operator<<(ostream& os, Element &a);
	int v_int;
	string v_string;
	double v_double;
public:
	Element() 
	{
		v_int = 0;
		v_string = "";
		v_double = 0.0;
	}
	Element(int a_int,string a_string,double a_double)
	{
		v_int = a_int;
		v_string = a_string;
		v_double = a_double;
	}
	Element(int a_int)
	{
		v_int = a_int;
		v_string ="";
		v_double =0.0;
	}
	Element( string a_string)
	{
		v_int = 0;
		v_string = a_string;
		v_double = 0.0;
	}
	Element( double a_double)
	{
		v_int = 0;
		v_string ="";
		v_double = a_double;
	}
	void set_int(int a_int)
	{
		v_int = a_int;
	}
	void set_string(string a_string)
	{
		v_string = a_string;
	}
	void set_double(double a_double)
	{
		v_double = a_double;
	}
	int get_int()
	{
		return v_int;
	}
	string get_string()
	{
		return v_string;
	}
	double get_double()
	{
		return v_double;
	}
	void* operator new[](size_t sz) {
		cout << "operator new[] called!!" << "(" << sz << " bytes)"
			<< endl;
		return malloc(sz);
	}
};

ostream& operator<<(ostream& os, Element &a)
{
	os << a.get_int() << endl;
	os << a.get_string() << endl;
	os << a.get_double() << endl;
	return os;
}