#pragma once
#include "Element.h"
class Register//class register created like singleton
{
private:
	static Register *s;
	Element *storage;//array of element for saving data
	unsigned int size;//size of array

	Register()//constructor in private
	{
		s = nullptr;
		storage = nullptr;
		size = 0;
	}
public:
	Register(Register const&) = delete;//make copyconstructor disable
	void operator=(Register const&) = delete;//make operator= disable
	static Register *get_instance()//static method  
	{
		if (!s)
			s = new Register;
		return s;
	}
	Element *get_storage()//return pointer to array of data
	{
		return storage;
	}
	void add( int a_int = 0, string a_string = "", double a_double=0.0)//adding element to array
	{
		Element *storage_new = new Element[size + 1];
		int i = 0;
	     for (i = 0; i < size; i++)
	     {
		   storage_new[i] = storage[i];
	     }
	if (i == size)
	{
		storage_new[i] = Element(a_int, a_string, a_double);
	}
	size++;
	delete[]storage;
	storage = storage_new;
		
	}
	//Error here!!!!!!!!!!
	/*Element &operator[](int i) {
		if (i > size) {
			cout << "Index out of bounds" << endl;
			// return first element.
			return storage[0];
		}
		return storage[i];
	}
	/*Element &operator[](string key) //Error here!!!!!!!!!!
	{
		for (int i = 0; i < size; i++)
		{
			if (storage[i].get_string = key)
			{
				cout << storage[i];
				return storage[i];
			}
	    }
		return storage[0];
	}*/
	
	int delete_element(int point_delete)//deleting element from array
	{
		if (point_delete<0 || point_delete>size - 1)
		{
			cout << "wrong number for deleting" << endl;
			return 0;
		}
		Element *storage_new = new Element[size - 1];
		int i = 0, j = 0;
		for (i = 0, j = 0; i < point_delete; i++, j++)
		{
			storage_new[i] = storage[j];
		}
		if (i == point_delete)
			j++;
		for (; i < size - 1; i++, j++)
		{
			storage_new[i] = storage[j];
		}
		size--;
		delete[]storage;
		storage = storage_new;
		return 1;
	}
	void show_storage()//show array of data use overloaded cout for Element
	{
		for (int i = 0; i < size; i++)
			cout << storage[i] << endl;
	}
};

