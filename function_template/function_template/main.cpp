#include <iostream>
#include <cstring>

using namespace std;

template <typename K, int size>
void bubleSort(K(&arr)[size])
{
	for (int b = 0; b < size - 1; b++) {
		for (int i = 0; i < size - 1; i++) {
			if (arr[i] > arr[i + 1]) {
				K j = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = j;
			}
		}
	}
}


template <typename T>
void print_array(const T *arr, int size)
{
	for (int i = 0; i < size; i++)
		cout << arr[i] << " ";
	cout << endl;
}
int main()
{
	const int iSize = 10,
		dSize = 7,
		fSize = 10,
		cSize = 5;
	// ������� ������ ����� ������
	int    iArray[iSize] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	double dArray[dSize] = { 1.2345, 2.234, 3.57, 4.67876, 5.346, 6.1545, 7.7682 };
	float  fArray[fSize] = { 1.34, 2.37, 3.23, 4.8, 5.879, 6.345, 73.434, 8.82, 9.33, 10.4 };
	char   cArray[cSize] = { "MARS" };

	cout << "\n int:\n";
	print_array(iArray, iSize);

	cout << "\n double:\n";
	print_array(dArray, dSize);

	cout << "\n float:\n"; 
	print_array(fArray, fSize);


	cout << "\n char:\n";
	print_array(cArray, cSize);

	cout << "------------------------" << endl;
	cout << "\n int:\n";
	bubleSort((&iArray)[iSize]);
	print_array(iArray, iSize);
	cout << endl;
	cout << "\n double:\n";
	bubleSort((&dArray)[dSize]);
	print_array(dArray, dSize);
	cout << endl;
	cout << "\n float:\n";
	bubleSort((&fArray)[fSize]);
	print_array(fArray, fSize);
	cout << endl;
	cout << "\n char:\n";
	bubleSort((&cArray)[cSize]);
	print_array(cArray, cSize);
	cout << endl;
}