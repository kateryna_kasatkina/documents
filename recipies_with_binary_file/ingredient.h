#pragma once
#include <string>
#include<iostream>
#include<fstream>
using namespace std;
class INGREDIENT
{
	//friend ostream& operator<<(ostream& os, INGREDIENT a);
private:
	string ing;
	int weight;
	INGREDIENT *next;
	INGREDIENT *prev;
public:

	INGREDIENT();

	INGREDIENT(string aName, int aWeight);

	INGREDIENT(INGREDIENT &a);
	
	void set_name_ing(string aName_ing);
	
	string get_name_ing();
	
	void set_weight(int aWeight);
	
	int get_weight();
	
	void show_ingredient();
	
	void set_next(INGREDIENT *aNext);

	INGREDIENT *get_next();
	
	void set_prev(INGREDIENT *aPrev);
	
	INGREDIENT *get_prev();
	void write_to_file_ingredient(fstream &f)
	{
		size_t size_ing = ing.size();
		//write to file the lenght of string
		f.write((char*)&size_ing, sizeof(size_ing));
		//write to file string
		f.write(ing.c_str(), size_ing);
		//write to file weight of ingredient (int)
		f.write(reinterpret_cast<char*>(&weight),sizeof(weight));
	}
	/*ifstream ifile("Notebook.dat", ios::binary);
 Notes  Note;			    // ����������������� ����������
 char str[80];			    // ����������� ����� ������
 // ��������� � ���������� ������ � �����, ���� �� eof
 while (!ifile.read((char*)&Note, sizeof(Notes)).eof()) {
  sprintf(str, "%s\t���: %s\t�������: %d",
   Note.Name, Note.Phone, Note.Age);
  cout << str << endl;
 }
 ifile.close();		    // ������� ����������� ����
 cin.sync(); cin.get(); return 0;
}*/
	void read_from_file(fstream &f)
	{
		//read the size of string
		size_t size_ing=0;
		//f.read((char*)&size_ing, sizeof(size_ing));
		f.read(reinterpret_cast<char*>(size_ing), sizeof(size_ing));
		cout << size_ing;
	}
};

/*ostream& operator<<(ostream& os, INGREDIENT a)
{
	os << a.get_name_ing() << " , ";
	os << a.get_weight() << "g" << endl;
	return os;
}*/