/*����������� ����� �������. ����� ������ ��������� ���������� � ������� ������������� ������������� �����.
������ ������ ����� ��������� ����:
-	��������;
-	��� ����� (��������/�������);
-	�������� �������� �������������;
-	������ ������������(������������ ������).
������ ���� ���������� ��������� ����������:
-	������/�������� �������� �������;
-	������/�������� ��� �����;
-	������/�������� �������� �������� �������������;
-	����������� �������� ����������(�������� � ��� � �������);
-	�������� ����������� �� ������;
-	���������� ���� ������ �������.

����������� ����������� ��������� ������ �������� � ������ ������������.

��� ��������� ��������������� ���� � �������� ���������� ����������� ��������� � ��������� ����������� ���������, ������� � ������������� �������/����������� � ��������� ������ � �������� ����.
*/
#include <iostream>
#include <fstream>
#include <string>
#include "book.h"
#include "recipe.h"
#include "ingredient.h"
using namespace std;
int main()
{
	
	RECIPE x("backed potatoe", "hot", "backe");
	x.add_ing("meat", 500);
	x.add_ing("onion", 100);
	x.show_recipe();
	cout << "------------------" << endl;

	RECIPE c(x);
	c.show_recipe();
	
	INGREDIENT vv("water", 500);
	fstream f("recipe_book.bin", ios::binary | ios::app);
	vv.write_to_file_ingredient(f);
	//c.write_to_file_recipe(f);
	
	f.close();

	ifstream show("recipe_book.bin", ios::binary | ios::in);
	vv.read_from_file(f);

	f.close();
}
