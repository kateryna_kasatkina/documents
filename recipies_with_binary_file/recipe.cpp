#include <iostream>
#include <string>
#include "book.h"
#include "recipe.h"
#include "ingredient.h"
using namespace std;
RECIPE::RECIPE()
{
	string a = "no data";
	name = a;
	type = a;
	prep = a;
	ingredients.head=nullptr;
	ingredients.tail = nullptr;
	quan_ing = 0;
}

RECIPE::RECIPE( string aName, string aType, string aPrep)
{
	name = aName;
	type = aType;
	prep = aPrep;
	ingredients.head = nullptr;
	ingredients.tail = nullptr;
	quan_ing = 0;
}

RECIPE::RECIPE(RECIPE &aRecipe)
{
	name = aRecipe.name;
	type = aRecipe.type;
	prep = aRecipe.prep;
	quan_ing = aRecipe.quan_ing;
	INGREDIENT *tmp = aRecipe.ingredients.get_head();
	while (tmp)
	{
		string n=tmp->get_name_ing();
		int w=tmp->get_weight();
		ingredients.add(n, w);
		tmp = tmp->get_next();
	}
	 
}
RECIPE::~RECIPE()
{
	ingredients.~double_linked_list();
}

void RECIPE::set_name_recipe(string aName)
{
	name = aName;
}
string RECIPE::get_name_recipe()
{
	return name;
}
void RECIPE::set_type_recipe(string aType)
{
	type = aType;
}
string RECIPE::get_type_recipe()
{
	return type;
}
void RECIPE::set_prep_recipe(string aPrep)
{
	prep = aPrep;
}
string RECIPE::get_prep_recipe()
{
	return prep;
}

void RECIPE::show_ingredients()
{
	ingredients.show();
}

void RECIPE::add_ing(string aName,int weight)
{
	ingredients.add(aName, weight);
	quan_ing++;
}

void RECIPE::delete_ing(int index)
{
	ingredients.del_by_index(index);
	quan_ing--;
	
}

void RECIPE::show_recipe()
{
	cout << "Name: " << name << endl;
	cout << "Type: " << type << endl;
	cout << "Preparation: " << prep << endl;
	cout << "Number of ingredients: " << quan_ing << endl;
	ingredients.show();

}