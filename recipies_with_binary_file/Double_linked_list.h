#pragma once
#include "ingredient.h"
#include "recipe.h"
class double_linked_list
{
public:
	
	INGREDIENT *head;
	INGREDIENT *tail;

public:
	double_linked_list()
	{
		head = tail = nullptr;
	
	}
	~double_linked_list()
	{
		while (head)
		{
			INGREDIENT *tmp_el = head;
			head = head->get_next();
			delete tmp_el;
		}
	}
	INGREDIENT *get_head()
	{
		return head;
	}
	INGREDIENT *get_tail()
	{
		return tail;
	}
	void add(string aName,int aWeight)
	{
		INGREDIENT*tmp_el = new INGREDIENT(aName, aWeight);

		if (head)
		{
			tail->set_next(tmp_el);
			tmp_el->set_prev(tail);
		}
		else
			head = tmp_el;
		tail = tmp_el;
	}

	void show()
	{
		INGREDIENT *tmp_el = head;
		while (tmp_el)
		{
			cout<<tmp_el->get_name_ing()<<" ";
			cout << tmp_el->get_weight() << "g" << endl;
			tmp_el = tmp_el->get_next();
		}
	}
	void show_reverse()
	{
		INGREDIENT *tmp_el = tail;
		while (tmp_el)
		{
			cout << tmp_el->get_name_ing() << " ";
			cout << tmp_el->get_weight() << "g" << endl;
			tmp_el = tmp_el->get_next();
		}
	}
	bool del_by_index(unsigned int index)
	{
		if (index)//���� ��� �������� ������!=0, ������ ��� �� �������������
		{
			INGREDIENT *prev = head;//��� ������ ���������� ����� ������
			for (int i = 0; i < index - 1 && prev; i++)
				prev = prev->get_next();//������� ������� �������������� �������

			INGREDIENT* tmp = nullptr;
			if (prev)
				tmp = prev->get_next();
			else
				tmp = NULL;


			if (tmp)//���� �� ����� �� ������ ������� ��� ��������
			{
				prev->set_next(tmp->get_next());
				if (tmp->get_next())
					tmp->get_next()->set_prev(prev);
				if (tmp == tail)
					tail = prev;
				delete tmp;
				
			}
		}
		else
		{
			INGREDIENT *tmp = head;
			head = head->get_next();
			delete tmp;
	
		}
		return true;
	}
	bool insertByIndex(int index,string aName, int aWeight) {
		if (index > 0) {

			INGREDIENT *prev = head;
			for (int i = 0; i<index - 1 && prev; i++)
				prev = prev->get_next();

			if (prev) {
				INGREDIENT *insElem = new INGREDIENT(aName, aWeight);
			
				insElem->set_next(prev->get_next());
				if (prev->get_next())
					prev->get_next()->set_prev(insElem);

				insElem->set_prev(prev);
				prev->set_next(insElem);

				if (prev == tail)  tail = insElem;
			}
			else return false;
		}
		else {
			 INGREDIENT *insElem = new INGREDIENT(aName, aWeight);
			
			insElem->set_next(head);
			head = insElem;
		}
		return true;
	}

};