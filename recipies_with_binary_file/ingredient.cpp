#include <iostream>
#include <string>
#include "book.h"
#include "recipe.h"
#include "ingredient.h"
using namespace std;

INGREDIENT::INGREDIENT()
{
	 ing = "no data";
	 weight = 0;
	 next = prev = nullptr;
}

INGREDIENT::INGREDIENT(string aName,int aWeight)
{
	ing = aName;
	 weight = aWeight;
	 next = prev = nullptr;
}

INGREDIENT::INGREDIENT(INGREDIENT &a)
{
	ing = a.ing;
	weight = a.weight;
	next = prev = nullptr;
}

void INGREDIENT::set_name_ing(string aName_ing)
{
	ing = aName_ing;
}
string INGREDIENT::get_name_ing()
{
	return ing;
}
void INGREDIENT::set_weight(int aWeight)
{
	weight = aWeight;
}
int INGREDIENT::get_weight()
{
	return weight;
}

void INGREDIENT::show_ingredient()
{
	cout << ing << " " << weight << "g" << endl;
}

void INGREDIENT::set_next(INGREDIENT *aNext)
{
	next = aNext;
}

INGREDIENT *INGREDIENT::get_next()
{
	return next;
}
void INGREDIENT::set_prev(INGREDIENT *aPrev)
{
	prev = aPrev;
}
INGREDIENT *INGREDIENT::get_prev()
{
	return prev;
}
