#pragma once
#include <iostream>
#include <string>
#include "ingredient.h"
#include "Double_linked_list.h"
class RECIPE
{
private:
	string name;
	string type;
	string prep;
	double_linked_list ingredients;
	int quan_ing;
public:

	RECIPE();
	
	RECIPE( string aName, string aType, string aPrep);

	RECIPE(RECIPE &aRecipe);
	
	void set_name_recipe(string aName);
	
	string get_name_recipe();
	
	void set_type_recipe(string aType);
	
	string get_type_recipe();
	
	void set_prep_recipe(string aPrep);
	
	string get_prep_recipe();

	void show_ingredients();
	
	void add_ing(string aName, int weight);
	
	void delete_ing(int index);
	
	~RECIPE();
	
	void show_recipe();

	void write_to_file_recipe(fstream &f)
	{
		size_t size_name = name.size();
		f.write((char*)&size_name, sizeof(size_name));
		f.write(name.c_str(), size_name);

		size_t size_type = type.size();
		f.write((char*)&size_type, sizeof(size_type));
		f.write(type.c_str(), size_type);


		size_t size_prep = prep.size();
		f.write((char*)&size_prep, sizeof(size_prep));
		f.write(prep.c_str(), size_prep);

		f.write(reinterpret_cast<char*>(&quan_ing), sizeof(quan_ing));

		INGREDIENT *tmp_el = ingredients.head;
		while (tmp_el)
		{
			tmp_el->write_to_file_ingredient(f);
			tmp_el = tmp_el->get_next();
		}
	}
};

