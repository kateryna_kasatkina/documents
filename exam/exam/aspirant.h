#pragma once
#include "school-student.h"
#include "student.h"
#include <iostream>
#include <fstream>
#include <exception>
using std::cout;
using std::endl;
using std::string;
using std::fstream;
class aspirant :public student
{
	friend ostream& operator<<(ostream& os, aspirant&a);
	friend istream& operator >> (istream& is, aspirant&a);
	int mark_of_science_vork;
public:
	aspirant(){mark_of_science_vork = 0;}
	aspirant(string aFirst, string aLast, int overal_mark, int aExam_mark,int aMark_of_science_vork):student(aFirst,aLast,overal_mark,aExam_mark), mark_of_science_vork(aMark_of_science_vork){}
	aspirant ( const aspirant &a)
	{
		firstname = a.firstname;
		lastname = a.lastname;
		overal_mark = a.overal_mark;
		exam_mark = a.exam_mark;
		mark_of_science_vork = a.mark_of_science_vork;

	}
	void set_mark_of_science_vork(int aMark_of_science_vork)
	{
		mark_of_science_vork = aMark_of_science_vork;
	}
	int get_mark_of_science_vork()
	{
		return mark_of_science_vork;
	}
	
	aspirant operator=(aspirant&a)
	{
		firstname = a.firstname;
		lastname = a.lastname;
		overal_mark = a.overal_mark;
		exam_mark = a.exam_mark;
		mark_of_science_vork = a.mark_of_science_vork;
		return *this;
	}
	bool operator==(aspirant&a)
	{
		if (firstname == a.firstname&&lastname == a.lastname&&overal_mark == a.overal_mark&&exam_mark == a.exam_mark&&mark_of_science_vork==a.mark_of_science_vork)
			return true;
		else
			return false;
	}
	bool operator!=(aspirant&a)
	{
		if (firstname != a.firstname || lastname != a.lastname || overal_mark != a.overal_mark || exam_mark != a.exam_mark || mark_of_science_vork != a.mark_of_science_vork)
			return true;
		else
			return false;
	}
	
	void write_to_file(fstream &f)
	{
		f.write(reinterpret_cast<char*>(&firstname), sizeof(firstname));
		f.write(reinterpret_cast<char*>(&lastname), sizeof(lastname));
		f.write(reinterpret_cast<char*>(&overal_mark), sizeof(overal_mark));
		f.write(reinterpret_cast<char*>(&exam_mark), sizeof(exam_mark));
		f.write(reinterpret_cast<char*>(&mark_of_science_vork), sizeof(mark_of_science_vork));
	}
};
ostream& operator<<(ostream& os, aspirant&a)
{
	os << "Firstname: " << a.firstname << endl;
	os << "Lastname: " << a.lastname << endl;
	os << "Overal mark: " << a.overal_mark << endl;
	os << "Exam mark: " << a.exam_mark << endl;
	os << "Mark of science work: " << a.mark_of_science_vork << endl;
	return os;
}
istream& operator >> (istream& is, aspirant&a)
{
	//cout << "input Firstname: " << endl;
	is >> a.firstname;
	//cout << "input Laststname: " << endl;
	is >> a.lastname;
	//cout << "input overal mark: " << endl;
	is >> a.overal_mark;
	//cout<<"input exam mark: "<<endl;
	is >> a.exam_mark;
	//cout <<"input mark of science work"<<endl;
	is >> a.mark_of_science_vork;
	return is;
}


