#include <iostream>
#include "Person.h"
#include "Tweeter.h"
#include <memory>//for shared pointers
using namespace std;
int main()
{
	Person *Kate = new Person("kate", "Gregory", 456);
	Tweeter *KateGregcons = new Tweeter("kate", "Gregory", 456,"@gregcons");
	Person *pKateGregcons = KateGregcons;
	cout << Kate->GetName() << endl;
	cout << KateGregcons->GetName() << endl;
	cout << pKateGregcons->GetName() << endl;

	//shared pointers
	//shared_ptr<Person> spKate = make_shared<Person>("sKate", "pGregory", 458);
	//auto spKate = make_shared<Person>("sKate", "pGregory", 458);
	//cout << spKate->GetName() << endl;


	shared_ptr<Person> spKate = make_shared<Tweeter>("sKate", "pGregory", 458,"@gregcons");
	delete KateGregcons;


}