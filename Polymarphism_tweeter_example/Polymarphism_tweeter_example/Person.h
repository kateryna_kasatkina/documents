#pragma once
#include <string>
using namespace std;
class Person
{
protected:
	string firstname;
	string lastname;
	int arbitrarynumber;
public:
	Person(string aFirst,string aLast,int aArbitrary):firstname(aFirst),lastname(aLast), arbitrarynumber(aArbitrary){}
	virtual ~Person(){}
	virtual string GetName()const
	{
		return firstname + " " + lastname;
	}
};