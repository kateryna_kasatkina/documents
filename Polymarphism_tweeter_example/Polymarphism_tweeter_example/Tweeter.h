#pragma once
#include <string>
#include "Person.h"
using namespace std;
class Tweeter :public Person
{
	string twitterhandle;
public:
	Tweeter(string aFirst, string aLast, int aArbitrary,string handle):Person(aFirst,aLast, aArbitrary),twitterhandle(handle){}
	~Tweeter(void){}
	string GetName()const
	{
		return Person::GetName() + " " + twitterhandle;
	}
};