#include <iostream>
using namespace std;
#include "Object.h"
#include "Ellipse.h"
#include "Rectangle.h"
void main()
{

	//Object obj1;
	Ellipse obj2;
	Rectangle obj3;

	//obj1.square();
	obj2.square();
	obj3.square();
	cout << " 2 ============================================"<<endl;

/*	
	Object arr[2];
	arr[0] = obj2;
	arr[1] = obj3;

	for(int i=0; i<2 ;i++)
		arr[i].square();

	cout << " 3 ============================================"<<endl;
	*/


	Object* ob_arr[3];

	//ob_arr[0] = new Object;
	ob_arr[1] = new Ellipse;
	ob_arr[2] = new Rectangle;

    for(int i=1; i<3; i++)
		ob_arr[i]->square();

	//((Ellipse*)ob_arr[1])->ellipse_method();

	for(int i=1; i<3; i++)
		delete ob_arr[i];

}