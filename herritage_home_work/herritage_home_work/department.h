#pragma once
#include <iostream>
#include <string>
#include <list>
#include <iterator>
#include <algorithm>
#include "date.h"
#include "employee.h"
using namespace std;
class department :public date
{
	friend ostream& operator<<(ostream& out, const department&a);
protected:
	date created;//date of creating
	date edit;//date of changing
	string name_department;//name of department
	list<employee> *employees;//��������� ����� ����������� � �������
	int size_employees;//number of employees in department

public:
	department():date(){}
	//overloaded constructor
	department(int c_day, int c_month, int c_year, int e_day, int e_month, int e_year, string aName_department):created(c_day, c_month, c_year), edit(e_day, e_month, e_year), name_department(aName_department), employees(nullptr), size_employees(0){}
	void set_name_department(string aName_department)
	{
		name_department = aName_department;
	}
	string get_name_department ()const
	{
		return name_department;
	}
	void set_date_created(int aDay,int aMonth,int aYear)
	{
		created.day = aDay;
		created.month = aMonth;
		created.year = aYear;
	}
	date get_date_created()const
	{
		return created;
	}
	void set_date_edit(int aDay, int aMonth, int aYear)
	{
		edit.day = aDay;
		edit.month = aMonth;
		edit.year = aYear;
	}
	date get_date_edit()const
	{
		return edit;
	}
	void set_size_employees(int aSize_employees)
	{
		size_employees = aSize_employees;
	}
	int get_size_employees()const
	{
		return size_employees;
	}
	//mothod adds employee to list
	void add_employee(employee &a)
	{
		if (employees == nullptr)
			employees = new list<employee>;
		employees->push_back(a);
		size_employees++;
	}
	//method delete employee from list
	void delete_employee(employee &a)
	{
		if (employees == nullptr)
			cout << "thre is no list of employees";
		employees->remove(a);
		size_employees--;

	}
	list<employee> *get_ptr_employee_department()
	{
		return employees;
	}
	bool operator==(const department&a)
	{
		if (created == a.created&&edit == a.edit&&name_department == a.name_department && size_employees == a.size_employees)
			return true;
		else
			return false;
	}

};


ostream& operator<<(ostream& out, const department&a)
{
	out << "date of creation: " << a.get_date_created();
	out << "date of edition: " << a.get_date_edit();
	out << "name of department: " << a.name_department ;
	copy(a.employees->begin(), a.employees->end(), ostream_iterator<employee>(cout, " ")); // ����� �� ����� ��������� ������
	out << "number of employees: " << a.size_employees;
	return out;
}