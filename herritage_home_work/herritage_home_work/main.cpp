/*����������� ����� ������������. ����� ������ ����� ����������� ���������,
������� � ������������� ������. ������ ������ ���� ����������� ���� ����������.
� ������ ������ ������ �������������� ���� �������� � ���� ���������� ��������������
������ ������. ���� ������ ���� ����������� ���� ���������� ������.

4 ����.
 ����������� �������� �������

6 �����.
 4 + �������� ����� ���������� � ����������� ��������� ����� ����������� � �������.
 ������ ������ ����������� ������ ���� �������� � � ������ �����������.

8 �����.
 6 + ��� �������� ������� ������������ ������������ ���������� ������.

10 �����.
 8 + ����������� ���������  > � < � ������� � ����������� � ������� ���������� � ������������,
 ������� � ����������� � ��������������� ����.
 */
#include <iostream>
#include <string>
#include <list>
#include <iterator>
#include <algorithm>
#include "date.h"
#include "department.h"
#include "employee.h"
#include "supermarket.h"
using namespace std;
int main()
{
	ostream_iterator<date> screen(cout, "");
	
	employee d_1("bob", "qqqqq", 6, 5, 4);
	employee d_2("Georg", "hhhhh", 6, 5, 4);
	department sweets;
	department vegetables(3, 5, 1999, 4, 6, 2000, "vegetables");
	sweets.set_date_created(1, 9, 2016);
	sweets.set_date_edit(2, 6, 20017);
	sweets.set_name_department("sweets");
	sweets.add_employee(d_1);
	sweets.add_employee(d_2);
	cout << "--------------------------------------\n";
	cout << sweets;
	sweets.delete_employee(d_1);
	cout << "--------------------------------------\n";
	cout << sweets<<endl;
	cout << "=============================================\n";
	supermarket klass("klass",0);
	klass.add_department(sweets);
	klass.add_department(vegetables);
	cout << "=============================================\n";
	//klass.delete_department(sweets);
	cout << klass;
	department c=klass.find_department_to_change(sweets);//function returns department for changing
	c.set_name_department("good_sweets");
	cout << c;
}
