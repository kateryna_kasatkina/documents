#pragma once
#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
using namespace std;
class date
{
	friend class department;
	friend class employee;
	friend ostream& operator<<(ostream& out, const date&a);
protected:
	int day;
	int month;
	int year;
public:
	date()//default constructor
	{
		day = 0;
		month = 0;
		year = 0;
	}
	date(int aDay,int aMonth,int aYear)//overloaded constructor
	{
		day = aDay;
		month = aMonth;
		year = aYear;
	}
	date(date const &aDate)//copy constructor
	{
		day = aDate.day;
		month = aDate.month;
		year = aDate.year;
	}
	//methods to set and get values
	void set_day(int aDay)
	{
		day = aDay;
	}
	void set_month(int aMonth)
	{
		month = aMonth;
	}
	void set_year(int aYear)
	{
		year = aYear;
	}
	int get_day()
	{
		return day;
	}
	int get_month()
	{
		return month;
	}
	int get_year()
	{
		return year;
	}
	//overloaded == to compare dates
	bool operator==(const date&a)
	{
		if (day == a.day&&month == a.month&&year == a.year)
			return true;
		else
			return false;
	}
};
//overloaded << operator for date
ostream& operator<<(ostream& out, const date&a)
{
	out << "date: " << a.day << "." << a.month << "." << a.year << endl;
	return out;
}