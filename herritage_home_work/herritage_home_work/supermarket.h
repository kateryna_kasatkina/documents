#pragma once
#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
#include "department.h"
class supermarket :public department
{
	friend ostream& operator<<(ostream& out, const supermarket&a);
protected:
	string name_supermarket;
	list<department> departments;
	list<employee> *all_employees;
	int size_departments;
public:
	supermarket()
	{
		name_supermarket = "";
		size_departments = 0;
	}
   supermarket(string aName_supermarket,int aSize_departments)//overloaded constructor
	{
		name_supermarket = aName_supermarket;
		size_departments = aSize_departments;
		all_employees = nullptr;
	}
	void set_name_supermarket(string aName)//set name of supermarket
	{
		name_supermarket = aName;
	}

	string get_name_supermarket()const//returns name of supermarket
	{
		return name_supermarket;
	}
	int get_size_department()const//returns number of departments
	{
		return size_departments;
	}
	void add_department(department &a)//add department to list
	{
		departments.push_back(a);
		size_departments++;
	}
	void delete_department(department &a)//delete department from list
	{
		departments.remove(a);
		size_departments--;
	}
	department find_department_to_change(department &a)//method returns department with posibility to change data
	{
		auto result = find(begin(departments), end(departments), a);
		department weLookedFor = *result;
		return weLookedFor;
	}
};

ostream& operator<<(ostream& out, const supermarket&a)
{
	out << "name of supermarket:"<<a.name_supermarket<<endl;
	//copy(a.departments.begin(), a.departments.end(), ostream_iterator<department>(cout, " "));
	//copy(a.all_employees->begin(), a.all_employees->end(), ostream_iterator<employee>(cout, " "));
	out <<"number of departments: "<< a.size_departments << endl;
	return out;
}