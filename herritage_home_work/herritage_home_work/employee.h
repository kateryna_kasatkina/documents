#pragma once
#include "date.h"
#include <string>
#include <iostream>

using namespace std;
class employee
{
	friend ostream& operator<<(ostream& out, const employee&a);//overloaded << for printing list
protected:
	string name;
	string surname;
	date birth;
public:
	employee()//default constructor
	{
		name = "";
		surname = "";
		birth.day = 0;
		birth.month = 0;
		birth.year = 0;
	}
	employee(string aName,string aSurname,int aDay,int aMonth,int aYear)//overloaded constructor
	{
		name = aName;
		surname = aSurname;
		birth.day = aDay;
		birth.month = aMonth;
		birth.year = aYear;
	}
	void set_name(string aName)//sets name of employee
	{
		name = aName;
	}
	void set_surname(string aSurname)//sets surname of employee
	{
		name = aSurname;
	}
	void set_date_birth(int aDay, int aMonth, int aYear)//sets date of birth
	{
		birth.day = aDay;
		birth.month = aMonth;
		birth.year = aYear;
	}
	string get_name_employee()//returns name of employee
	{
		return name;
	}
	string get_surname_employee()//returns surname of employee
	{
		return surname;
	}
	date get_date_birth_employee()//returns date of birth
	{
		return birth;
	}
	bool operator==(const employee&a)//overloaded operator to compare employees
	{
		if (name == a.name&&surname == a.surname&&birth==a.birth)
			return true;
		else
			return false;
	}
};

ostream& operator<<(ostream& out, const employee&a)//overloaded << for printing list
{
	out << endl << endl;
	out << "name of employee : " << a.name << endl;
	out << "surname of employee : " << a.surname << endl;
	out << "date of birth: " << a.birth << endl;
	return out;
}

