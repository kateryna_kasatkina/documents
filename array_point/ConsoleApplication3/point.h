#pragma once
#include <iostream>
#include <ctime>
#include "array1.h"
using namespace std;
class point
{
	int x;
	int y;
public:
	point();
	
	point(int aX, int aY);
	
	point(const point &a);
	
	void show_point();
	
	point operator=(const point &apoint);
	
	point operator+(const point &apoint);
	
	point operator+(int value);
	
	bool operator==(const point &apoint);

	friend void search(int s_arr[3], const int s_size);
};