#include <iostream>
#include "point.h"
using namespace std;
point::point()
{
	x = rand() % (9 - 1 + 1) + 1;
	y = rand() % (9 - 1 + 1) + 1;

}
point::point(int aX, int aY)
{
	x = aX;
	y = aY;
}
point::point(const point &a)
{
	x = a.x;
	y = a.y;
}
void point::show_point()
{
	cout << "(" << x << "," << y << ")" << endl;
}
point point::operator=(const point &apoint)
{
	x = apoint.x;
	y = apoint.y;
	return *this;
}

point point::operator+(const point &apoint)
{
	point result(x + apoint.x, y + apoint.y);
	return result;
}
point point::operator+(int value)
{
	x = value + x;
	y = value + y;
	return *this;
}
 bool point::operator==(const point &apoint)
{
	if ((x == apoint.x) && (y == apoint.y))
		return true;
	else
		return false;
}