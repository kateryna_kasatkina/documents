#include <iostream>
#include <map>
#include <iterator>
#include <algorithm>

using std::map;
using std::cout;
using std::endl;
using std::back_inserter;
using std::insert_iterator;
using std::pair;
using std::generate_n;


pair<int, int> gen1() {
	return pair<int, int>(rand() % 10, rand() % 100);
}

void main() {
	map<int, int> m1;

	insert_iterator<map<int, int>> it(m1, m1.begin());
	
	/*for (int i = 0; i < 10; i++) {
		auto p = pair<int, int>(rand() % 10, rand() % 100);
		*it = p;
		//it++;

	}*/
	generate_n(inserter(m1, m1.begin()), 20, gen1);
	generate_n(it, 20, gen1);

	for (auto i : m1) {
		cout << "[" << i.first << "]"<< i.second << endl;
	}


}