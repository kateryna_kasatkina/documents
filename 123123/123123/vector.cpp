#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using std::vector;
using std::cout;
using std::endl;
using std::back_inserter;
using std::generate_n;
using std::sort;
using std::less;
using std::greater;



void main() {
	vector<int> v1;


	generate_n(back_inserter(v1), 20, rand);
	
	for (auto i : v1) {
		cout <<  i << " ";
	}
	cout << endl;
	sort(v1.begin()+5, v1.end()-5, greater<int>());
	

	for (auto i : v1) {
		cout << i << " ";
	}
	cout << endl;


}