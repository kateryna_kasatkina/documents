#pragma once
#include <string>
#include "Transport.h"
using namespace std;
class water_transport :public Transport
{
protected:
	string place_location;
public:
	water_transport() {}
	water_transport(string aName, string aType, string aUnits, string aPlace_location) :Transport(aName, aType, aUnits), place_location(aPlace_location){}
	virtual ~water_transport() {}
	virtual string get_units_of_speed()
	{
		return name_of_transport + " " + units_of_speed;
	}
	virtual string get_type_of_transport()
	{
		return type_of_transport;
	}
	virtual void show_info()
	{
		cout << name_of_transport << endl;
		cout << type_of_transport << endl;
		cout << units_of_speed << endl;
		cout << place_location << endl;
		cout << "------------------------------\n";
	}
};