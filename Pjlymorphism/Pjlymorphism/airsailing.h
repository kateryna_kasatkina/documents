#pragma once
#include <string>
#include "Transport.h"
#include "air_transport.h"
using namespace std;
class airsailing :public air_transport
{
protected:
	int number_of_sportmen;

public:
	airsailing() {}
	airsailing(string aName, string aType, string aUnits, string aClass_of_pilot, int aNumber_of_sportmen) :air_transport(aName, aType, aUnits, aClass_of_pilot), number_of_sportmen(aNumber_of_sportmen) {}
	virtual ~airsailing() {}
	virtual string get_units_of_speed()
	{
		return name_of_transport + " " + units_of_speed;
	}
	virtual string get_type_of_transport()
	{
		return type_of_transport;
	}
	virtual void show_info()
	{
		cout << name_of_transport << endl;
		cout << type_of_transport << endl;
		cout << units_of_speed << endl;
		cout << class_of_pilot << endl;
		cout << number_of_sportmen << endl;
		cout << "------------------------------\n";
	}
};
