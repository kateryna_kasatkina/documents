#pragma once
#include <string>
#include "Transport.h"
#include "ground_transportation.h"
using namespace std;
class mototransport :public ground_transportation
{
	string kind;
	string engine_capacity;
	int  weight;
public:
	mototransport(string aName, string aType, string aUnits, string aClass_of_safety, string aKind, string aEngine_capacity,int aWeight) :ground_transportation(aName, aType, aUnits, aClass_of_safety), kind(aKind), engine_capacity(aEngine_capacity),weight(aWeight) {}
	~mototransport() {}
	virtual string get_units_of_speed()
	{
		return name_of_transport + " " + kind + " " + units_of_speed;
	}
	virtual string get_kind_of_transport()
	{
		return kind;
	}
	virtual int get()
	{
		return weight;
	}
	virtual void show_info()
	{
		cout << name_of_transport << endl;
		cout << type_of_transport << endl;
		cout << units_of_speed << endl;
		cout << class_of_safety << endl;
		cout << kind <<endl;
		cout << engine_capacity << endl;
		cout << weight << endl;
		cout << "------------------------------\n";
	}
};
