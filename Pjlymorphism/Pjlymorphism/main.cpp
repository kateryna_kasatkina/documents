/*������� �������� �������: ��������� ��������  ������  ���������
������������� �������������    ���������      ���������        �������     ���������������
������� � ������ ������� �� 2 ������. 
� ������ ������ ������ �������������� ������ ����������� ����������� ���������� � 
������������ ��������. ����������� ��������� ����� ������������ ������� ��������� 
�������� ������� ������������� ��������.
*/

#include <iostream>
#include <string>
#include <memory>
#include "Transport.h"
#include "ground_transportation.h"
#include "autotransport.h"
#include "mototransport.h"
#include "water_transport.h"
#include "underwater_transport.h"
#include "air_transport.h"
#include "aviation.h"
#include "airsailing.h"
using namespace std;
int main()
{
	//using shared pointers (dynamic polymorhpism)
	shared_ptr<Transport> test_1 = make_shared<Transport>("teat_name", "test_type", "test-units");
	test_1->show_info();
	shared_ptr<Transport> test_2 = make_shared<ground_transportation>("teat name", "test type", "test units","test class");
	test_2->show_info();
	shared_ptr<Transport> test_3 = make_shared<autotransport>("teat name", "test type", "test units", "test class","test autotransport", "1.6");
	test_3->show_info();
	shared_ptr<Transport> test_4 = make_shared<mototransport>("teat name", "test type", "test units", "test class", "test autotransport", "1.6",195);
	test_4->show_info();

	shared_ptr<Transport> test_5 = make_shared<water_transport>("boat","water","nodes","Black Sea");
	test_5->show_info();
	shared_ptr<Transport> test_6 = make_shared<underwater_transport>("boat", "water", "nodes", "Black Sea",277);
	test_6->show_info();
	shared_ptr<Transport> test_7 = make_shared<underwater_transport>("boat", "water", "nodes", "Black Sea", 20);
	test_7->show_info();
	shared_ptr<Transport> test_8 = make_shared<air_transport>("plane", "boing", "km/hour", "A");
	test_8->show_info();
	shared_ptr<Transport> test_9 = make_shared<aviation >("plane", "boing", "km/hour", "A",12);
	test_9->show_info();
	shared_ptr<Transport> test_10 = make_shared<airsailing>("plane", "boing", "km/hour", "A", 2);
	test_10->show_info();
}
