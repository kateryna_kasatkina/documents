#pragma once
#include <string>
#include "Transport.h"
using namespace std;
class air_transport :public Transport
{
protected:
	string class_of_pilot;
public:
	air_transport() {}
	air_transport(string aName, string aType, string aUnits, string aClass_of_pilot) :Transport(aName, aType, aUnits), class_of_pilot(aClass_of_pilot){}
	virtual ~air_transport() {}
	virtual string get_units_of_speed()
	{
		return name_of_transport + " " + units_of_speed;
	}
	virtual string get_type_of_transport()
	{
		return type_of_transport;
	}
	virtual void show_info()
	{
		cout << name_of_transport << endl;
		cout << type_of_transport << endl;
		cout << units_of_speed << endl;
		cout << class_of_pilot << endl;
		cout << "------------------------------\n";
	}
};