#pragma once
#include <string>
#include "Transport.h"
#include "water_transport.h"
using namespace std;
class onwater_transport :public water_transport
{
protected:
	int number_of_workers;

public:
	onwater_transport() {}
	onwater_transport(string aName, string aType, string aUnits, string aPlace_location, int aNumber_of_workers) :water_transport(aName, aType, aUnits, aPlace_location), number_of_workers(aNumber_of_workers) {}
	virtual ~onwater_transport() {}
	virtual string get_units_of_speed()
	{
		return name_of_transport + " " + units_of_speed;
	}
	virtual string get_type_of_transport()
	{
		return type_of_transport;
	}
	virtual void show_info()
	{
		cout << name_of_transport << endl;
		cout << type_of_transport << endl;
		cout << units_of_speed << endl;
		cout << place_location << endl;
		cout << number_of_workers << endl;
		cout << "------------------------------\n";
	}
};
