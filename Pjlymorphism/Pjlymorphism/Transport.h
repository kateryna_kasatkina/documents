#pragma once
#include <iostream>
#include <string>
using namespace std;
class Transport
{
	friend ostream& operator<<(ostream& os, Transport&a);
protected:
	string name_of_transport;
	string type_of_transport;
	string units_of_speed;
public:
	Transport()
	{
		name_of_transport = "";
		type_of_transport = "";
		units_of_speed = "";
	}
	Transport(string aName, string aType, string aUnits)
	{
		name_of_transport = aName;
		type_of_transport = aType;
		units_of_speed = aUnits;
	}
	virtual ~Transport(){}
	virtual string get_units_of_speed()
	{
		return units_of_speed;
	}
	virtual string get_type_of_transport()
	{
		return type_of_transport;
	}
	virtual void show_info()
	{
		cout << name_of_transport << endl;
		cout << type_of_transport << endl;
		cout << units_of_speed << endl;
		cout << "------------------------------\n";
	}
};

ostream& operator<<(ostream& os, Transport&a)
{
	os << "name_of_transport: " << a.name_of_transport << endl;
	os << "type_of_transport: " << a.type_of_transport << endl;
	os << "units_of_speed: " << a.units_of_speed << endl;
	return os;
}