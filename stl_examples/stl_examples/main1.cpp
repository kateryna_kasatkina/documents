#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
using namespace std;
int main()
{
	vector<int> v{ 4, 6, 6, 1, 3, -2, 0, 11, 2, 3, 2, 4, 4, 2, 4};
	string s{ "Hello I am a sentence" };
	//find the first zero in the colection
	auto result = find(begin(v), end(v), 0);
	int WeLookedFor = *result;
	cout << WeLookedFor << endl;
	//find the first 2 after 0
	result = find(result, end(v), 2);
	if (result != end(v))
	{
		WeLookedFor = *result;
	}

	//find the first a
	auto letter = find(begin(s), end(s), 'a');
	char a = *result;
	//cout << a;


	//find first odd value
	result = find_if(begin(v), end(v), [](auto elem) {return elem % 2 != 0; });
	WeLookedFor = *result;
	cout << "find_if: " << WeLookedFor << endl;
	//find first even value
	result=find_if_not(begin(v), end(v), [](auto elem) {return elem % 2 != 0; });
	WeLookedFor = *result;
	cout << "find_if_not: " << WeLookedFor << endl;


	vector<int> primes{ 1,2,3,5,7,11,13 };
	result = find_first_of(begin(v), end(v), begin(primes), end(primes));
	WeLookedFor = *result;
	cout << "first prime in vector: " << WeLookedFor << endl;

	vector<int>subsequence{ 2,4 };
	result = search(begin(v), end(v), begin(subsequence), end(subsequence));
	WeLookedFor = *result;
	
	string am = "am";
	letter = search(begin(s), end(s), begin(am), end(am));
	a = *letter;
	cout << a<<endl;


	result = search_n(begin(v), end(v), 2, 4);//we are looking for two fours

	result = adjacent_find(begin(v), end(v));//searching for two numbers the same in entire vector

}