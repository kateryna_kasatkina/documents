#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <random>
#include <iterator>
#include <math.h>
#include <numeric>
#include<functional>
#include "employee.h"
#include "resourse.h"
using namespace std;
/*int main()
{ //////////////////////////SORT////////////////////////////////////////
	vector <int> v{ 4,1,0,1,-2,3,7,-6,2,0,0,-9,9 };
	auto v2 = v;
	for (size_t i = 0; i < v2.size(); i++)
	{
		cout << v2[i] << " ";
	}
	cout << endl;
	sort(begin(v2), end(v2));
	for (size_t i = 0; i < v2.size(); i++)//from small to big
	{
		cout << v2[i] << " ";
	}
	cout << endl;
	sort(begin(v2), end(v2), [](int elem1, int elem2) {return elem1 > elem2; });//from big to small
	for (size_t i = 0; i < v2.size(); i++)
	{
		cout << v2[i] << " ";
	}
	cout << endl;
	sort(begin(v2), end(v2), [](int elem1, int elem2) {return abs(elem1) > abs(elem2); });//abs= absolute value 
	for (size_t i = 0; i < v2.size(); i++)
	{
		cout << v2[i] << " ";
	}
	cout << endl;

	///////////////////////STABLE SORT/////////////////////////////////////
 
	vector<employee> staff{
	{"kate","gregory",1000},
	{"Obvius","Artificial",2000},
	{"Fake","name",1000},
	{"alan","turing",2000},
	{"Grase","hopper",2000},
	{"Anita","Bork",2000}};

	sort(begin(staff), end(staff), [](employee e1, employee e2) {return e1.get_salary() < e2.get_salary(); });
	for (size_t i = 0; i < staff.size(); i++)
	{
		cout << staff[i] << " ";
	}
	cout << endl;
	cout << "==================================" << endl;
	sort(begin(staff), end(staff), [](employee e1, employee e2) {return e1.get_sorting_name() < e2.get_sorting_name(); });
	for (size_t i = 0; i < staff.size(); i++)
	{
		cout << staff[i] << " ";
	}
	cout << endl;

	cout << "==================================" << endl;//
	sort(begin(staff), end(staff), [](employee e1, employee e2) {return e1.get_sorting_name() < e2.get_sorting_name(); });
	stable_sort(begin(staff), end(staff), [](employee e1, employee e2) {return e1.get_salary() < e2.get_salary(); });
	for (size_t i = 0; i < staff.size(); i++)
	{
		cout << staff[i] << " ";
	}
	cout << endl;

	////////////////////is it sorted?///////////////////////
	auto sorted = is_sorted(begin(v2), end(v2));
	sorted=is_sorted(begin(v2), end(v2), [](int elem1, int elem2) {return abs(elem1) > abs(elem2); });

	////////////////////the largest and the smallest//////////////////////
	int high = *(max_element(begin(v), end(v)));
	int low= *(min_element(begin(v), end(v)));
	sort(begin(v2), end(v2));
	low = *begin(v2);
	cout << "low: " << low << endl;
	high = *(end(v2) - 1);
	cout << "high: " << high << endl;
	int positive = *upper_bound(begin(v), end(v), 0);//find first element that greate than 0
	cout << "positive: " << positive << endl;

	////////////////////////////shuffling(���������)////////////////////////////////
	random_device randomdevice;
	mt19937 generator(randomdevice());
	shuffle(begin(v2), end(v2), generator);//�� ���������������� ������� ���������� ��������������


	////////////////////////partial sorting/////////////////////////////////
	partial_sort(begin(v2), find(begin(v2), end(v2), 4), end(v2));

	

}*/


/////////////////comparing///////////////////////////////////
/*int main()
{
	vector<int>a{ 1,2,3,4,5 };
	vector<int>b{ 1,2,0,4 };
	bool same = a.size() == b.size();
	same = equal(begin(a), end(a), begin(b), end(b));
	cout << same;


	////////////////////////////TOTAL//////////////////////////
	vector<string> words{ "one","two","three" };
	auto allwords = accumulate(begin(words), end(words), string{});
	int lenghtwords = allwords.size();
	allwords = accumulate(begin(words), end(words), string{ "words: " }, [](const string total, string& s) {return total + " " + s; });
	cout << allwords << endl;



	vector<int> k{ 1,1,1,1,1,1,1 };
	for (auto i : k)
	{
		cout << k[i] << " ";
	}
	cout << endl;
	for_each(begin(k), end(k), [](int &elem) { elem = 2; });//������� �������� ��������� 2
	for (auto i : k)
	{
		cout << k[i] << " ";
	}
	cout << endl;

	vector<int> b{ 1,2,3,4,5,6,7 };
	auto firsttree = find(begin(b), end(b), 3);
	for_each(firsttree, end(b), [](int &elem) { elem = 2; });

}*/
//int main()
//{///////////////////////COPING/////////////////////////////////
	//vector<int>sourse{ 3,6,1,0,-2,5 };
	//vector<int> w2(sourse.size());
	//copy(begin(sourse), end(sourse), begin(w2));
	//for (size_t i=0;i<w2.size();i++)
	//{
	//	cout << w2[i] << " ";
	//}
	//cout << endl;
	//auto w3 = sourse;
	//for (size_t i = 0; i<w3.size(); i++)
	//{
	//	cout << w3[i] << " ";
	//}
	//cout << endl;

	//auto position = find(begin(sourse), end(sourse), -2);//������� ������� �������
	//vector<int> w4(sourse.size());
	//copy(begin(sourse), position, begin(w4));
	//for (size_t i = 0; i<w4.size(); i++)
	//{
	//	cout << w4[i] << " ";
	//}
	//cout << endl;

	//vector<int> w5(sourse.size());
	//copy_if(begin(sourse), end(sourse), begin(w5), [](int elem) {return elem % 2 == 0; });//�������� ������ ������
	//for (size_t i = 0; i<w5.size(); i++)
	//{
	//	cout << w5[i] << " ";
	//}
	//cout << endl;

	////copy_n �������� ��������� ���������� ���������
	//copy_n(begin(w5), 3, begin(w2));//�� ������� w5 ������ ��� �������� ����������� � w2
	//for (size_t i = 0; i<w2.size(); i++)
	//{
	//	cout << w2[i] << " ";
	//}
	//cout << endl;


	///////////////////////REMOVING  ELEMENTS////////////////////////////////
	//cout << "Before removing: " << endl;
	//for (size_t i = 0; i<sourse.size(); i++)
	//{
	//	cout << sourse[i] << " ";
	//}
	//cout << endl;
	//auto newend = remove(begin(sourse), end(sourse), 3);
	//int s = sourse.size();
	//int logicalsize = newend - begin(sourse);
	////cout << "logicalsize: " << logicalsize << endl;
	//cout << "after removing: " << endl;
	//for (size_t i = 0; i<sourse.size(); i++)
	//{
	//	cout << sourse[i] << " ";
	//}
	//cout << endl;
	//sourse.erase(newend, end(sourse));
	//cout << "after arase: " << endl;
	//for (size_t i = 0; i<sourse.size(); i++)
	//{
	//	cout << sourse[i] << " ";
	//}
	//cout << endl;

	//sourse = w3;
	//sourse.erase(remove(begin(sourse), end(sourse), 3), end(sourse));//������� 3-�� � ������� � ������� � �������

	//vector<resourse> vr(2);//������ �� 2 �������
	//vr[0].set_value(8);
	//cout << vr[0] << endl;
	//vr[1].set_value(9);
	//cout << vr[1] << endl;
	////vr.insert(8);
	//
	//auto newend2 = remove_if(begin(vr), end(vr), [](const resourse& r) {return r.get_value() == 8; });
	//vr.erase(newend2, end(vr));
	//


	//////////////////////CREATING AND FILLING COLLECTIONS///////////////////////////////
	//vector<int> v6(10);
	//fill(begin(v6), end(v6), 1);//������ ������� 1
	//for (size_t i = 0; i<v6.size(); i++)
	//	{
	//	    cout << v6[i] << " ";
	//	}
	//cout << endl<<"==============================\n";
	//fill_n(begin(v6), 6, 2);//������ 6-�� ������� 2-�� ������ 1-��
	//for (size_t i = 0; i<v6.size(); i++)
	//{
	//	cout << v6[i] << " ";
	//}
	//cout << endl<<"==============================\n";
	//iota(begin(v6), end(v6), 1);
	//for (size_t i = 0; i<v6.size(); i++)
	//{
	//	cout << v6[i] << " ";
	//}
	//cout << endl<<"==============================\n";


	//int index = 10;
	//generate(begin(v6), end(v6), [&index]() {return --index; });//�� 9 �� 0
	//for (size_t i = 0; i<v6.size(); i++)
	//{
	//	cout << v6[i] << " ";
	//}
	//cout << endl << "==============================\n";
	//auto sourse = v6;
	//index = 1;
	//generate_n(begin(v6), 7, [&index]() {return (index *= 2); });//�������� � ������� 7 ����������
	//for (size_t i = 0; i<v6.size(); i++)
	//{
	//	cout << v6[i] << " ";
	//}
	//cout << endl << "==============================\n";

	//replace(begin(v6), end(v6), 2, 7);//����� ����� 2 �������� �� 7
	//for (size_t i = 0; i<v6.size(); i++)
	//{
	//	cout << v6[i] << " ";
	//}
	//cout << endl << "==============================\n";
	//replace_if(begin(v6), end(v6), [](int elem) {return elem < 16; }, 7);//���� ������� ������ 16 �������� �� 7
	//for (size_t i = 0; i<v6.size(); i++)
	//{
	//	cout << v6[i] << " ";
	//}
	//cout << endl << "==============================\n";



//}
