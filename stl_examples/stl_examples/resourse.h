#pragma once
#include <iostream>
using namespace std;
class resourse {
public:
	friend ostream& operator<<(ostream& os, resourse&a);
	int i=1;
    int objectcount=0;
public:
	resourse() {
		i = 1;
		objectcount++;
	}
	resourse(int aI) {
		i = aI;
		objectcount++;
	}
	resourse(const resourse &r) {
		i = r.i;
		objectcount ++;
	}
	resourse& operator=(const resourse &r)
	{
		i = r.i;
		objectcount++;
		return *this;
	}
	void set_value(int aI)
	{
		i = aI;
	}
	int get_value()const
	{
		return i;
	}
	~resourse()
	{
		objectcount--;
	}
};
ostream& operator<<(ostream& os, resourse&a)
{
	cout << "Value: "<<a.get_value() << endl;
	cout << "Count: "<<a.objectcount << endl;
	return os;
}