#pragma once
#include <string>
using namespace std;
class employee {
	friend ostream& operator<<(ostream& os, employee&a);
private:
	string firstname;
	string lastname;
	int salary;
public:
	employee(string aFirstname, string aLastname, int aSalary) :firstname(aFirstname), lastname(aLastname), salary(aSalary) {}
	int get_salary()
	{
		return salary;
	}
	string get_sorting_name()
	{
		return lastname + ", " + firstname;
	}
	string get_firstname()
	{
		return firstname;
	}
	string get_lastname()
	{
		return lastname;
	}
};
ostream& operator<<(ostream& os, employee&a)
{
	os << "firstname: " << a.get_firstname()<<" ";
	os << "lastname: " << a.get_lastname()<<" " ;
	os << "salary: " << a.get_salary() << endl;
	return os;
}