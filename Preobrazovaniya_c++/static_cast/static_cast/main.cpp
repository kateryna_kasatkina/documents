#include <iostream>

using std::cout;
using std::endl;

class BaseClass {
public:
	
	void f( ) const {
		cout << "BASE\n"; 
	}
};
//=================================
class DerivedClass: public BaseClass {
	int t;
public:
	DerivedClass() {
		t = 0;
	}
	void f( ) const { 
		cout << "DERIVED   " << t << "\n"; 
	}
};
//============================================================
void test( BaseClass * );

int main ()
{
	// ������������� static_cast ��� ��������������
	double d = 8.22;
	int x = static_cast< int >( d ) ;
	cout <<"d is " << d << "\nx is " << x << endl;

	

	BaseClass * basePtr = new DerivedClass;
	test( basePtr ); // call test
	test(new BaseClass);
	delete basePtr;

	return 0;
}

void test( BaseClass * basePtr )
{
	DerivedClass *derivedPtr;

	// �������������� ��������� �������� ������
	// � ��������� ������������ ������
	//derivedPtr = (DerivedClass *)( basePtr );
	//derivedPtr = basePtr; // !!!!error
	derivedPtr = static_cast< DerivedClass * >( basePtr );
	derivedPtr->f(); //����� ������� f ������ DerivedClass
}