#pragma once
#include "object.h"

class Rectangle :	public Object
{
	int rect_prop;
public:
	Rectangle(void);
	virtual double square();
	int get_rect_prop();
	
public:
	~Rectangle(void);
};
