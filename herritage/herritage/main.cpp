#include <iostream>
using namespace std;
class f1
{
protected:
	int value;
public:
	f1()
	{
		value = 0;
	}
	f1(int input)
	{
		value = input;
	}
	void show_value()
	{
		cout << value << endl;
	}
};
class f2:public f1
{
public:
   f2():f1(){}
   f2(int inputS):f1(inputS){}
   void value_sqr()
   {
	   value *= value;
   }
   void show()
   {
	   if (value != 0)
		   f1::show_value();
   }
};
int main()
{
	f1 x(3);
	x.show_value();

	f2 y(4);
	y.show_value();
	 
	y.value_sqr();
	y.show_value();
	y.show();
}