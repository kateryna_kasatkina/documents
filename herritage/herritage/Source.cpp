#include <iostream>
using namespace std;
class unit
{
protected:
	int health;
public:
	unit()
	{
		health = 10;
	}
	unit(int ahealth)
	{
		health = ahealth;
	}
	void show_health()
	{
		cout << "health: " << health << endl;
	}
};
class soldier :virtual public unit
{
private:
	int damage;
public:
	soldier()
	{
		damage = 20;
	}
	soldier(int adamage) :damage(adamage){}
	soldier(int ahealth,int adamage):unit(ahealth),damage(adamage){}
	void show_damage()
	{
		cout << "damage: " << damage << endl;
	}
};
class horse:virtual public unit
{
private:
	int speed;
public:
horse():speed(20){}
void show_speed()
{
	cout <<"horse speed: "<< speed << endl;
}
};
class horseman :public horse, public soldier
{};
int main()
{
	//unit s(20);
	//s.show_health();
	/*soldier x(30,12);
	x.show_damage();
	x.show_health();
	horse n;
	n.show_speed();
	n.show_health();*/
	horseman mm;
	mm.show_damage();
	mm.show_health();
	mm.show_speed();
}