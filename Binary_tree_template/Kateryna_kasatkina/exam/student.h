#pragma once
#include "school-student.h"
#include <iostream>
using std::cout;
using std::endl;
using std::string;
class student :public school_student
{
	friend ostream& operator<<(ostream& os, student&a);
	friend istream& operator >> (istream& is, student&a);
protected:
	int exam_mark;
public:
	student()
	{
		exam_mark = 0;
	}
	student(string aFirst, string aLast, int overal_mark,int aExam_mark):school_student(aFirst,aLast,overal_mark), exam_mark(aExam_mark){}
	student(student &a)
	{
		firstname = a.firstname;
		lastname = a.lastname;
		overal_mark = a.overal_mark;
		exam_mark = a.exam_mark;
	}
	void set_exam_mark(int aExam_mark)
	{
		exam_mark = aExam_mark;
	}
	int get_exam_mark()const
	{
		return exam_mark;
	}
	student operator=(student&a)
	{
		firstname = a.firstname;
		lastname = a.lastname;
		overal_mark = a.overal_mark;
		exam_mark = a.exam_mark;
		return *this;
	}
	bool operator==(student&a)
	{
		if (firstname == a.firstname&&lastname == a.lastname&&overal_mark == a.overal_mark&&exam_mark==a.exam_mark)
			return true;
		else
			return false;
	}
	bool operator!=(student&a)
	{
		if (firstname != a.firstname || lastname != a.lastname || overal_mark != a.overal_mark|| exam_mark != a.exam_mark)
			return true;
		else
			return false;
	}
};
ostream& operator<<(ostream& os, student&a)
{
	os << "Firstname: " << a.firstname << endl;
	os << "Lastname: " << a.lastname << endl;
	os << "Overal mark: " << a.overal_mark << endl;
	os << "Exam mark: " << a.exam_mark << endl;
	return os;
}
istream& operator >> (istream& is, student&a)
{
	//cout << "input Firstname: " << endl;
	is >> a.firstname;
	//cout << "input Laststname: " << endl;
	is >> a.lastname;
	//cout << "input overal mark: " << endl;
	is >> a.overal_mark;
	//cout<<"input exam mark: "<<endl;
	is >> a.exam_mark;

	return is;
}