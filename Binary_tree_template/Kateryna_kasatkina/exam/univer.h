#pragma once
#include "school-student.h"
#include "student.h"
#include "aspirant.h"
#include <vector>
#include <algorithm>
#include <list>
using std::cout;
using std::endl;
using std::vector;
using std::string;
using std::list;
class univer 
{
	vector<aspirant> cafedra;
	int number_of_aspirants = 0;
	//list <student> group;
public:
	void add_aspirant(aspirant &a)
	{
		cafedra.push_back(a);
		number_of_aspirants++;
	}
	void remove_aspirant(string aFirstname)
	{
		auto new_cafedra = remove_if(begin(cafedra), end(cafedra), [&aFirstname](aspirant &a) {return a.get_firstname() == aFirstname; });
		cafedra.erase(new_cafedra, end(cafedra));
		number_of_aspirants--;
	}
	void show_aspirants()
	{
		for (size_t i = 0; i<cafedra.size(); i++)
			{
				cout << cafedra[i] << " ";
			}
	}
	int change_name_aspirant(string aFirstname)
	{
		auto ptr = find_if(begin(cafedra), end(cafedra), [&aFirstname](aspirant &a) {return a.get_firstname() == aFirstname; });
		ptr->set_firstname(aFirstname);
	}
};
