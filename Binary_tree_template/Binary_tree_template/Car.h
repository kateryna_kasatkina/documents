#pragma once
#include <string>
using namespace std;
class Car
{
	friend ostream& operator<<(ostream&os, Car &aCar);
	int ID; //ID ����(�� ��������) ��������� �� ������� 1, 2...
	string model;

public:
	
	
	Car()		//default constructor
	{
		ID = 0;
		model = "";
	}
	Car(int aID, string aModel)//overloaded constructor
	{
		ID = aID;
		model = aModel;
	}
	Car(const Car &aCar)//copy constructor
	{
		ID = aCar.ID;
		model = aCar.model;
	}
	~Car()				//destructor
	{

	}
	//set ��������� ���������� ����� ��� �������� ��� ������������ ��������*/
	void set_ID(int aID)				//to set or change ID of a car
	{
		ID = aID;

	}
	void set_model(string aModel)	//to set or change model
	{
		model = aModel;
	}
	int get_ID()const
	{
		return ID;
	}
	string get_name()
	{
		return model;
	}
	string get_model()const
	{
		return model;
	}
	bool operator>(Car a)
	{
		if (model > a.get_model())
			return true;
		else
			return false;
	}
	bool operator<(Car a)
	{
		if (model < a.get_model())
			return true;
		else
			return false;
	}
};

ostream& operator<<(ostream& os, Car &a)
{
	os << "model: " << a.get_model() << endl;
	os << "ID: " << a.get_ID() << endl;
	return os;
}