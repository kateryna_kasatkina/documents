#pragma once
#include <string>
#include <iomanip>
#include "Element .h"
using namespace std;
template <typename N>
class tree
{
	Element<N> *root;
public:

	tree()//default constructor
	{
		root = nullptr;
	}
	void print()//method passes root to print_helper for printig tree
	{
		print_helper(root);
	}
	int print_helper(Element<N> *el)//prints tree min-max
	{

		if (el->get_left())
			print_helper(el->get_left());
		cout <<( el->get_value());
		if (el->get_right())
			print_helper(el->get_right());

		return 0;
	}

	void minTree()//method passes root to minTree_helper for searching minimum of tree
	{
		minTree_helper(root);
	}

	Element<N> *minTree_helper(Element<N> *rt)//search minimum of tree
	{
		if (rt == NULL) return NULL;
		while (rt->get_left())
			rt = rt->get_left();
		cout << "Minimum: " << endl;
		cout<< (*rt);
		return rt;
	}
	void maxTree()//method passes root to maxTree_helper for searching maximum of tree
	{
		maxTree_helper(root);
	}
	Element<N> *maxTree_helper(Element<N> *rt)//search maximum of tree
	{
		while (rt->get_right())
			rt = rt->get_right();
		cout << "Maximum: " << endl;
		cout<< (*rt);
		return rt;
	}

	Element<N>* look_up(string key)//passes root to look_up_helper
	{
		return look_up_helper(root, key);
	}
	Element<N> *look_up_helper(Element<N> *rt, string key)//search the node by key(name)
	{
		if (rt == nullptr)
			return rt;
		if (rt->get_value().get_name() == key)//seach by name
		{
			cout << (*rt);
			return rt;
		}
		else
		{
			if (rt->get_value().get_name() < key)
				return look_up_helper(rt->get_right(), key);
			else
				return look_up_helper(rt->get_left(), key);
		}
		return 0;
	}
	//adding node to the tree	
	void add(N aValue)// ���������� �������� � ������	
	{
		if (!root)
			root = new Element<N>(aValue);
		else
		{
			Element<N> *tmp = root;
			while (tmp)
			{
				if (aValue < tmp->get_value())
					if (tmp->get_left() == NULL)
					{
						tmp->set_left(new Element<N>(aValue));
						break;
					}
					else tmp = tmp->get_left();
				else
					if (aValue > tmp->get_value())
						if (tmp->get_right() == NULL)
						{
							tmp->set_right(new Element<N>(aValue));
							break;
						}
						else tmp = tmp->get_right();

					else tmp = NULL; // break; // return;
			}
		}
	}
	void del_element(N aValue)
	{
		Element<N> *tmp = root;
		Element<N> *parent = NULL;
		if (root && root->get_value() == aValue)
		{ // root delete
		  //tmp = root;
			if (root->get_left() == NULL && root->get_right() == NULL) {
				delete root;
				root = NULL;
				return;
			}
			else {
				if (root->get_left() != NULL && root->get_right() != NULL) {
					Element<N>* min = root->get_right();
					root = root->get_right();
					while (min->get_left())
						min = min->get_left();
					min->set_left(tmp->get_left());

				}
				else {
					if (root->get_left())
						root = root->get_left();
					else
						root = root->get_right();
				}
				delete tmp;
			}
		}
		else
			while (tmp) {
				if (aValue< tmp->get_value())
					if (tmp->get_left() != NULL) {
						parent = tmp;
						tmp = tmp->get_left();
					}
					else {
						// no element found
						return;
					}
				else
					if (aValue> tmp->get_value())
						if (tmp->get_right() != NULL) {
							parent = tmp;
							tmp = tmp->get_right();
						}
						else {
							// no element found
							return;
						}
					else if (aValue == tmp->get_value()) {
						if (tmp->get_left() == NULL && tmp->get_right() == NULL) {
							if (parent->get_left() == tmp)
								parent->set_left(NULL);
							else
								parent->set_right(NULL);
							delete tmp;
							tmp = NULL;
						}
						else if (tmp->get_left() != NULL && tmp->get_right() != NULL) {
							if (parent->get_left() == tmp)
								parent->set_left(tmp->get_right());
							else
								parent->set_right(tmp->get_right());
							Element<N>* min = tmp->get_right();
							while (min->get_left())
								min = min->get_left();
							min->set_left(tmp->get_left());
							delete tmp;
							tmp = NULL;
						}
						else {
							if (parent->get_left() == tmp)
								parent->set_left(tmp->get_left() ? tmp->get_left() : tmp->get_right());
							else
								parent->set_right(tmp->get_left() ? tmp->get_left() : tmp->get_right());
							delete tmp;
							tmp = NULL;
						}
					}
			}
	}
	
};

