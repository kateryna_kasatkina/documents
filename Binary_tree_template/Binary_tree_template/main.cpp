/*������� ����������, ������� ��������� ��������� ��������� ������. � �������� ���������� ������ ���������� ������������ 
��������� �������� ������. 
���� 1. �������������� �������. ��������� ������, ����������� ������ � ������� ��������������.
���� 2. �������� ��������� ����� ��������� ������, ������� ����� ������� ����� ��������.
 - ����������� ����������� ����������, ��������, �������������� ������.
 - ����������� ����� � �������� ������.
���� 3. ��������� ���������� �� ������� ������� �������, ����������, AudioCD..
���� 4. ����������� ��������� ��� ���������� �������.
���� 5. ����������� ���������� � ������ ������ � �������� �����.
*/
#include <iostream>
#include <string>
#include "tree.h"
#include "student.h"
#include "Element .h"
#include "Car.h"
using namespace std;
int main()
{
	//creating the students
	student x ("bob", "grey", 6);
	student y("pop", "blue", 7);
	student p("plya", "lolia", 12);
	//creating binary tree using template class
	tree <student> *Tree=new tree<student>;
	//adding elements
	Tree->add(x);
	Tree->add(y);
	Tree->add(p);
	
	cout << "search for maximum" << endl;
	cout << "===============" << endl;
	Tree->maxTree();//search for maximum
	cout << endl << endl;
	cout << "search for minimum" << endl;
	cout << "===============" << endl;
	Tree->minTree();//search for minimum
	cout << endl << endl;
	cout << "print tree: " << endl;
	cout << "===============" << endl;
	Tree->print();//printing the tree
	cout << endl << endl;
	cout << "after deleting" << endl;
	cout << "===============" << endl;
	Tree->del_element(x);
	Tree->print();//printing the tree
	cout << endl << endl;
	string h = "pop";// name for searching
	cout << "serching for the element with such name and get a pointer" << endl;
	cout << "===============" << endl;
	Element<student> *el_4=Tree->look_up(h);//serching for the element with such name and get a pointer
	student xx=el_4->get_value();//get value (student) by pointer from method  look_up
	cout << "change data in element(student)" << endl;
	cout << "===============" << endl;
	xx.set_name("cccccc");//change data in element(student)
	cout << xx;
	cout << endl << endl << endl;
	



	Car dd(3, "supercar");
	Car vv(2, "taksebecar");
	tree <Car> *Tree_1 = new tree<Car>;
	Tree_1->add(dd);
	Tree_1->add(vv);
	cout << "search for maximum" << endl;
	cout << "===============" << endl;
	Tree_1->maxTree();//search for maximum
	cout << "search for minimum" << endl;
	cout << "===============" << endl;
	Tree_1->minTree();//search for minimum
	
	cout << "print tree: " << endl;
	cout << "===============" << endl;
	Tree_1->print();//printing the tree

	cout << "serching for the element with such name and get a pointer" << endl;
	cout << "===============" << endl;
	string hh = "supercar";// name for searching
	Element<Car> *car_4 = Tree_1->look_up(hh);
	Car xxhh = car_4->get_value();
	cout << "change data in element(student)" << endl;
	cout << "===============" << endl;
	xxhh.set_model("wow");
	cout << xxhh;
}