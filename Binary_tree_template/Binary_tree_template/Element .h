#pragma once
#include "student.h"
#include "Car.h"
template <typename N>
class Element
{
	friend ostream& operator<<(ostream& os, Element &a);
	Element *left;
	Element *right;
	N value;
public:
	Element()
	{
		left = nullptr;
		right = nullptr;
	
	}
	Element(N a)
	{
		left = nullptr;
		right = nullptr;
		value = a;
	}
	void set_left(Element *aLeft)
	{
		left = aLeft;
	}
	void set_right(Element *aRight)
	{
		right = aRight;
	}
	void set_value(N aValue)
	{
		value = aValue;
	}
	Element *get_left()
	{
		return left;
	}
	Element *get_right()
	{
		return right;
	}
	N get_value()
	{
		return value;
	}
};

ostream& operator<<(ostream& os, Element<student> &a)
{
	os << "Element: " << endl;
	os<< a.get_value() << endl;
	return os;
}

ostream& operator<<(ostream& os, Element<Car> &a)
{
	os << "Element: " << endl;
	os << a.get_value() << endl;
	return os;
}