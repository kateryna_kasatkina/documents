#pragma once
#include <string>
using namespace std;
class student
{
	friend ostream& operator<<(ostream& os, student &a);
	string name;
	string surname;
	int general_mark;
public:
	student()
	{
		name = "";
		surname = "";
		general_mark = 0;
	}
	student(string aName, string aSurname, int aGeneral_mark)
	{
		name = aName;
		surname = aSurname;
		general_mark = aGeneral_mark;
	}
	void set_name(string aName)
	{
		name= aName;
	}
	void set_surname(string aSurname)
	{
		surname = aSurname;
	}
	void set_general_mark(int aGeneral_mark)
	{
		general_mark = aGeneral_mark;
	}
	string get_name()
	{
		return name;
	}
	string get_surname()
	{
		return surname;
	}
	int get_general_mark()
	{
		return general_mark;
	}
	bool operator>(student aStudent)
	{
		if (general_mark > aStudent.get_general_mark())
			return true;
		else
			return false;
		
	}
	bool operator<(student aStudent)
	{
		if (general_mark < aStudent.get_general_mark())
		    return true;
		else
			return false;
	}
	bool operator==(student aStudent)
	{
		if (general_mark == aStudent.get_general_mark() && name == aStudent.get_name() && surname == aStudent.get_surname())
			return true;
		else
			return false;
	}
};

ostream& operator<<(ostream& os, student &a)
{
	os << "name: "<<a.get_name() << endl;
	os <<"surname: "<< a.get_surname() << endl;
	os << "mark: "<<a.get_general_mark()<< endl;
	return os;
}

